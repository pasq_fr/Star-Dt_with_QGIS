# StaR-Dt - fichiers version perso

> <mark>**ATTENTION**, vous n'êtes pas sur le site officiel mais sur un FORK pour personnalisation des éléments. Tout ce qui est dans ce site et le blog ne sont que mes remarques et interprétations très personnelles et n'engage que moi. Les fichiers sont fournis à titre d'exemple ou exercice personnel, je vous invite à n'utiliser que les liens et fichiers officiels, je ne peux (et ne veux) être tenus responsables de toutes pertes de données ou mauvaise utilisation de ces fichiers. Pour toutes démarches officielle, veuillez vous rendre sur <https://github.com/cnigfr/StaR-DT></mark>

"Le StaR-DT est un géostandard de description simplifiée des réseaux relevant de la réglementation anti-endommagement, à savoir l’arrêté du 15 février 2012 modifié en application du décret DT-DICT (Décret n° 2011-1241 du 5 octobre 2011 et ses décrets modificatifs), cité dans le document sous son nom courant Décret DT-DICT." Source CNIG

Retrouver l'article et les liens officiels correspondants :

*  pasq.fr
* [page du CNIG](http://cnig.gouv.fr/?page_id=11745)

# Contenu des dossiers

Scripts SQL
: contient le script SQL de création de la base postgis. Moyen de créer les gabarits en geopackage en respectant les héritages et les liaisons.

gabarits
: contient les gabarits en forme  géopackage + le projet QGIS

Codelistes_modele
: contient  les fichiers .csv (UTF-8, séparateur ; )  issue des fichiers XML fournis par le standard
          
Codelistes_on_steroids
: contient les fichiers .csv  modifiés et augmentés pour être utilisables sous qgis suivant parfois ma propre interprétation et [liste d'INSPIRE](https://inspire.ec.europa.eu/codelist)
          
Styles
: Contient les styles QGIS pour chaque couche. Dans le projet, les différents styles sont enregistrés dans les couches.

Styles --> paramètres
: contient : la palette de couleur, les symboles de lignes, et les texte de substition pour les étiquettes

Styles --> symboles
: contient les symboles en format SVG tel que défini dans les [documents accompagnant le StaR-DT.](http://cnig.gouv.fr/wp-content/uploads/2019/11/ANNEXE-Symbologie.pdf)
