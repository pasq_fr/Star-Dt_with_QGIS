<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="0" simplifyMaxScale="1" simplifyDrawingTol="1" maxScale="0" simplifyLocal="1" readOnly="0" labelsEnabled="0" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" version="3.16.16-Hannover" minScale="100000000">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal accumulate="0" startExpression="" endExpression="" startField="" durationField="" mode="0" enabled="0" endField="" durationUnit="min" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" type="categorizedSymbol" attr="typeaccessoire" forceraster="0" enableorderby="0">
    <categories>
      <category value="antenna" label="Antenne" render="true" symbol="0"/>
    </categories>
    <symbols>
      <symbol name="0" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
        <layer locked="0" class="SvgMarker" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="13,234,19,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="symbol/poi_tower_communications.svg" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="3" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol name="0" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="145,82,45,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </source-symbol>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="&quot;fid&quot;" key="dualview/previewExpressions"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" direction="0" opacity="1" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" penWidth="0" height="15" penAlpha="255" backgroundAlpha="255" enabled="0" scaleBasedVisibility="0" minScaleDenominator="0" barWidth="5" lineSizeType="MM" rotationOffset="270" spacing="5" sizeType="MM" scaleDependency="Area" spacingUnit="MM" width="15" maxScaleDenominator="1e+08" penColor="#000000" diagramOrientation="Up" spacingUnitScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" showAxis="1">
      <fontProperties description="Roboto,11,-1,5,25,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
      <axisSymbol>
        <symbol name="" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
          <layer locked="0" class="SimpleLine" pass="0" enabled="1">
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" dist="0" linePlacementFlags="18" priority="0" showAll="1" placement="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="identifiant" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionxy" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionz" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="xyschematique" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="visiblesurface" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sensible" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="statut" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="functional" name="Actif" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="projected" name="En projet" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underConstruction" name="En cours de construction/modification" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="decommissioned" name="Déclassé" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validede" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validejusque" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="positionverticale" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="onGroundSurface" name="au niveau du sol" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="suspendedOrElevated" name="suspendu ou surelevé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underground" name="sous le sol" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="miseajour" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dimension" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="caracteristiques" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="typeaccessoire" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="antenna" name="Antenne" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="copperMaintenanceLoop" name="boucle locale de maintenance" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="copperRepeater" name="répéteur en cuivre" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="digitalCrossConnect" name="répartiteur numérique (DXC)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="digitalLoopCarrier" name="portage à boucle numérique (DLC)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="exchange" name="echangeur" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="fiberInterconnect" name="interconnection de fibre" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="jointClosure" name="fermeture" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="loadCoil" name="bobine de charge" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="mainDistributionFrame" name="Répartiteur téléphonique (MDF)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="multiplexer" name="Multiplexeur (MUX)." type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="opticalMaintenanceLoop" name="boucle de maintenance optique" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="opticalRepeater" name="répétiteur optique" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="patchPanel" name="panneau de brassage" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="spliceClosure" name="fermeture d'épissure" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="splitter" name="répartiteur" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="terminal" name="borne" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="termination" name="terminaison" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="noticeBoard" name="panneau d'affichage" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="idouvrage" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="fid" index="0"/>
    <alias name="" field="identifiant" index="1"/>
    <alias name="" field="code" index="2"/>
    <alias name="" field="precisionxy" index="3"/>
    <alias name="" field="precisionz" index="4"/>
    <alias name="" field="xyschematique" index="5"/>
    <alias name="" field="visiblesurface" index="6"/>
    <alias name="" field="sensible" index="7"/>
    <alias name="" field="statut" index="8"/>
    <alias name="" field="validede" index="9"/>
    <alias name="" field="validejusque" index="10"/>
    <alias name="" field="positionverticale" index="11"/>
    <alias name="" field="miseajour" index="12"/>
    <alias name="" field="dimension" index="13"/>
    <alias name="" field="caracteristiques" index="14"/>
    <alias name="" field="typeaccessoire" index="15"/>
    <alias name="" field="idouvrage" index="16"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="identifiant" expression="" applyOnUpdate="0"/>
    <default field="code" expression="" applyOnUpdate="0"/>
    <default field="precisionxy" expression="" applyOnUpdate="0"/>
    <default field="precisionz" expression="" applyOnUpdate="0"/>
    <default field="xyschematique" expression="" applyOnUpdate="0"/>
    <default field="visiblesurface" expression="" applyOnUpdate="0"/>
    <default field="sensible" expression="" applyOnUpdate="0"/>
    <default field="statut" expression="'functional'" applyOnUpdate="0"/>
    <default field="validede" expression="" applyOnUpdate="0"/>
    <default field="validejusque" expression="" applyOnUpdate="0"/>
    <default field="positionverticale" expression="'underground'" applyOnUpdate="0"/>
    <default field="miseajour" expression="now()" applyOnUpdate="1"/>
    <default field="dimension" expression="" applyOnUpdate="0"/>
    <default field="caracteristiques" expression="" applyOnUpdate="0"/>
    <default field="typeaccessoire" expression="'Type d''accessoire'" applyOnUpdate="0"/>
    <default field="idouvrage" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="fid" constraints="3" unique_strength="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="identifiant" constraints="1" unique_strength="0" notnull_strength="1"/>
    <constraint exp_strength="0" field="code" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionxy" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionz" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="xyschematique" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="visiblesurface" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="sensible" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="statut" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validede" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validejusque" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="positionverticale" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="miseajour" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="dimension" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="caracteristiques" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="typeaccessoire" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="idouvrage" constraints="1" unique_strength="0" notnull_strength="1"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="identifiant" desc="" exp=""/>
    <constraint field="code" desc="" exp=""/>
    <constraint field="precisionxy" desc="" exp=""/>
    <constraint field="precisionz" desc="" exp=""/>
    <constraint field="xyschematique" desc="" exp=""/>
    <constraint field="visiblesurface" desc="" exp=""/>
    <constraint field="sensible" desc="" exp=""/>
    <constraint field="statut" desc="" exp=""/>
    <constraint field="validede" desc="" exp=""/>
    <constraint field="validejusque" desc="" exp=""/>
    <constraint field="positionverticale" desc="" exp=""/>
    <constraint field="miseajour" desc="" exp=""/>
    <constraint field="dimension" desc="" exp=""/>
    <constraint field="caracteristiques" desc="" exp=""/>
    <constraint field="typeaccessoire" desc="" exp=""/>
    <constraint field="idouvrage" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="identifiant" hidden="0" type="field" width="-1"/>
      <column name="code" hidden="0" type="field" width="-1"/>
      <column name="precisionxy" hidden="0" type="field" width="-1"/>
      <column name="precisionz" hidden="0" type="field" width="-1"/>
      <column name="xyschematique" hidden="0" type="field" width="-1"/>
      <column name="visiblesurface" hidden="0" type="field" width="-1"/>
      <column name="sensible" hidden="0" type="field" width="-1"/>
      <column name="statut" hidden="0" type="field" width="-1"/>
      <column name="validede" hidden="0" type="field" width="-1"/>
      <column name="validejusque" hidden="0" type="field" width="-1"/>
      <column name="positionverticale" hidden="0" type="field" width="-1"/>
      <column name="miseajour" hidden="0" type="field" width="-1"/>
      <column name="dimension" hidden="0" type="field" width="-1"/>
      <column name="caracteristiques" hidden="0" type="field" width="-1"/>
      <column name="idouvrage" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column name="fid" hidden="0" type="field" width="-1"/>
      <column name="typeaccessoire" hidden="0" type="field" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="typeaccessoire" showLabel="1" index="15"/>
    <attributeEditorField name="caracteristiques" showLabel="1" index="14"/>
    <attributeEditorField name="dimension" showLabel="1" index="13"/>
    <attributeEditorField name="xyschematique" showLabel="1" index="5"/>
    <attributeEditorField name="precisionxy" showLabel="1" index="3"/>
    <attributeEditorField name="precisionz" showLabel="1" index="4"/>
    <attributeEditorField name="visiblesurface" showLabel="1" index="6"/>
    <attributeEditorField name="sensible" showLabel="1" index="7"/>
    <attributeEditorField name="statut" showLabel="1" index="8"/>
    <attributeEditorField name="positionverticale" showLabel="1" index="11"/>
    <attributeEditorField name="validede" showLabel="1" index="9"/>
    <attributeEditorField name="validejusque" showLabel="1" index="10"/>
    <attributeEditorField name="miseajour" showLabel="1" index="12"/>
    <attributeEditorField name="identifiant" showLabel="1" index="1"/>
    <attributeEditorField name="idouvrage" showLabel="1" index="16"/>
    <attributeEditorField name="code" showLabel="1" index="2"/>
  </attributeEditorForm>
  <editable>
    <field name="caracteristiques" editable="1"/>
    <field name="code" editable="1"/>
    <field name="commentaire" editable="1"/>
    <field name="dimension" editable="1"/>
    <field name="dispositifprotection" editable="1"/>
    <field name="ecoulement" editable="1"/>
    <field name="exemptionic" editable="1"/>
    <field name="fid" editable="0"/>
    <field name="hauteurminreg" editable="1"/>
    <field name="hierarchie" editable="1"/>
    <field name="identifiant" editable="1"/>
    <field name="idouvrage" editable="1"/>
    <field name="materiau" editable="1"/>
    <field name="miseajour" editable="0"/>
    <field name="positionverticale" editable="1"/>
    <field name="precisionxy" editable="1"/>
    <field name="precisionz" editable="1"/>
    <field name="profondeurminnonreg" editable="1"/>
    <field name="profondeurminreg" editable="1"/>
    <field name="sensible" editable="1"/>
    <field name="statut" editable="1"/>
    <field name="typeaccessoire" editable="1"/>
    <field name="typecanalisationeau" editable="1"/>
    <field name="typedepart" editable="1"/>
    <field name="typeelement" editable="1"/>
    <field name="validede" editable="1"/>
    <field name="validejusque" editable="1"/>
    <field name="visiblesurface" editable="1"/>
    <field name="xyschematique" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="caracteristiques" labelOnTop="0"/>
    <field name="code" labelOnTop="0"/>
    <field name="commentaire" labelOnTop="0"/>
    <field name="dimension" labelOnTop="0"/>
    <field name="dispositifprotection" labelOnTop="0"/>
    <field name="ecoulement" labelOnTop="0"/>
    <field name="exemptionic" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="hauteurminreg" labelOnTop="0"/>
    <field name="hierarchie" labelOnTop="0"/>
    <field name="identifiant" labelOnTop="0"/>
    <field name="idouvrage" labelOnTop="0"/>
    <field name="materiau" labelOnTop="0"/>
    <field name="miseajour" labelOnTop="0"/>
    <field name="positionverticale" labelOnTop="0"/>
    <field name="precisionxy" labelOnTop="0"/>
    <field name="precisionz" labelOnTop="0"/>
    <field name="profondeurminnonreg" labelOnTop="0"/>
    <field name="profondeurminreg" labelOnTop="0"/>
    <field name="sensible" labelOnTop="0"/>
    <field name="statut" labelOnTop="0"/>
    <field name="typeaccessoire" labelOnTop="0"/>
    <field name="typecanalisationeau" labelOnTop="0"/>
    <field name="typedepart" labelOnTop="0"/>
    <field name="typeelement" labelOnTop="0"/>
    <field name="validede" labelOnTop="0"/>
    <field name="validejusque" labelOnTop="0"/>
    <field name="visiblesurface" labelOnTop="0"/>
    <field name="xyschematique" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties>
    <field name="ecoulement">
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties" type="Map">
          <Option name="dataDefinedAlias" type="Map">
            <Option value="false" name="active" type="bool"/>
            <Option value="1" name="type" type="int"/>
            <Option value="" name="val" type="QString"/>
          </Option>
        </Option>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </field>
  </dataDefinedFieldProperties>
  <widgets/>
  <previewExpression>"fid"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
