<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="1" simplifyMaxScale="1" simplifyDrawingTol="1" maxScale="0" simplifyLocal="1" readOnly="0" labelsEnabled="1" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" version="3.16.16-Hannover" minScale="100000000">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal accumulate="0" startExpression="" endExpression="" startField="" durationField="" mode="0" enabled="0" endField="" durationUnit="min" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" type="RuleRenderer" forceraster="0" enableorderby="0">
    <rules key="{380f5fe3-686e-4a22-914c-081b908899e8}">
      <rule label="Actif" filter="&quot;statut&quot; = 'functional'" key="{c9918b70-d316-40ad-b5b0-d56e0998a5e7}">
        <rule label="souterrain" filter=" &quot;positionverticale&quot; =  'underground' " symbol="0" key="{acc27ab9-003a-4810-a8bf-ffa0f53fa778}"/>
        <rule label="aérien" filter=" &quot;positionverticale&quot;  ='suspendedOrElevated' " symbol="1" key="{b52b94ef-b7ff-43de-9d35-d9630ad806bf}"/>
      </rule>
      <rule description="En projet / en cours de construction" label="En projet / en cours de construction" filter=" &quot;statut&quot; IN( 'projected' , 'underConstruction' )" symbol="2" key="{14c26db1-1297-4033-8ca8-0be96b76bb2d}"/>
      <rule label="Déclassé" filter=" &quot;statut&quot; = 'decommissioned' " symbol="3" key="{d81c4eae-aa83-4386-8478-ef0f73d6fbf6}"/>
      <rule label="erreurs" filter="ELSE" symbol="4" key="{eab0cf2a-2623-417e-a5de-2b47f598feb0}"/>
    </rules>
    <symbols>
      <symbol name="0" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="round" k="joinstyle"/>
          <prop v="234,13,205,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.7" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="MarkerLine" pass="0" enabled="1">
          <prop v="4" k="average_angle_length"/>
          <prop v="3x:0,0,0,0,0,0" k="average_angle_map_unit_scale"/>
          <prop v="MM" k="average_angle_unit"/>
          <prop v="25" k="interval"/>
          <prop v="3x:0,0,0,0,0,0" k="interval_map_unit_scale"/>
          <prop v="MapUnit" k="interval_unit"/>
          <prop v="0" k="offset"/>
          <prop v="0" k="offset_along_line"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_along_line_map_unit_scale"/>
          <prop v="MM" k="offset_along_line_unit"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="interval" k="placement"/>
          <prop v="0" k="ring_filter"/>
          <prop v="1" k="rotate"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <symbol name="@0@1" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
            <layer locked="0" class="FontMarker" pass="0" enabled="1">
              <prop v="0" k="angle"/>
              <prop v="A" k="chr"/>
              <prop v="234,13,205,255" k="color"/>
              <prop v="Arial Black" k="font"/>
              <prop v="Regular" k="font_style"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="miter" k="joinstyle"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="0,0,0,255" k="outline_color"/>
              <prop v="0.2" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="3" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties" type="Map">
                    <Option name="char" type="Map">
                      <Option value="true" name="active" type="bool"/>
                      <Option value="precisionxy" name="field" type="QString"/>
                      <Option value="2" name="type" type="int"/>
                    </Option>
                  </Option>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol name="1" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="round" k="joinstyle"/>
          <prop v="234,13,205,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.7" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="1" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="MarkerLine" pass="0" enabled="1">
          <prop v="4" k="average_angle_length"/>
          <prop v="3x:0,0,0,0,0,0" k="average_angle_map_unit_scale"/>
          <prop v="MM" k="average_angle_unit"/>
          <prop v="25" k="interval"/>
          <prop v="3x:0,0,0,0,0,0" k="interval_map_unit_scale"/>
          <prop v="MapUnit" k="interval_unit"/>
          <prop v="0" k="offset"/>
          <prop v="0" k="offset_along_line"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_along_line_map_unit_scale"/>
          <prop v="MM" k="offset_along_line_unit"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="interval" k="placement"/>
          <prop v="0" k="ring_filter"/>
          <prop v="1" k="rotate"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <symbol name="@1@1" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
            <layer locked="0" class="FontMarker" pass="0" enabled="1">
              <prop v="0" k="angle"/>
              <prop v="A" k="chr"/>
              <prop v="234,13,205,255" k="color"/>
              <prop v="Arial Black" k="font"/>
              <prop v="Regular" k="font_style"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="miter" k="joinstyle"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="0,0,0,255" k="outline_color"/>
              <prop v="0.2" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="3" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties" type="Map">
                    <Option name="char" type="Map">
                      <Option value="true" name="active" type="bool"/>
                      <Option value="precisionxy" name="field" type="QString"/>
                      <Option value="2" name="type" type="int"/>
                    </Option>
                  </Option>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol name="2" clip_to_extent="1" type="line" alpha="0.5" force_rhr="0">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="round" k="joinstyle"/>
          <prop v="234,13,205,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="8" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="HashLine" pass="0" enabled="1">
          <prop v="4" k="average_angle_length"/>
          <prop v="3x:0,0,0,0,0,0" k="average_angle_map_unit_scale"/>
          <prop v="MM" k="average_angle_unit"/>
          <prop v="35" k="hash_angle"/>
          <prop v="8" k="hash_length"/>
          <prop v="3x:0,0,0,0,0,0" k="hash_length_map_unit_scale"/>
          <prop v="MM" k="hash_length_unit"/>
          <prop v="5" k="interval"/>
          <prop v="3x:0,0,0,0,0,0" k="interval_map_unit_scale"/>
          <prop v="MM" k="interval_unit"/>
          <prop v="0" k="offset"/>
          <prop v="0" k="offset_along_line"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_along_line_map_unit_scale"/>
          <prop v="MM" k="offset_along_line_unit"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="interval" k="placement"/>
          <prop v="0" k="ring_filter"/>
          <prop v="1" k="rotate"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <symbol name="@2@1" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
            <layer locked="0" class="SimpleLine" pass="0" enabled="1">
              <prop v="0" k="align_dash_pattern"/>
              <prop v="square" k="capstyle"/>
              <prop v="5;2" k="customdash"/>
              <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
              <prop v="MM" k="customdash_unit"/>
              <prop v="0" k="dash_pattern_offset"/>
              <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
              <prop v="MM" k="dash_pattern_offset_unit"/>
              <prop v="0" k="draw_inside_polygon"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="234,13,205,255" k="line_color"/>
              <prop v="solid" k="line_style"/>
              <prop v="0.26" k="line_width"/>
              <prop v="MM" k="line_width_unit"/>
              <prop v="0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="0" k="ring_filter"/>
              <prop v="0" k="tweak_dash_pattern_on_corners"/>
              <prop v="0" k="use_custom_dash"/>
              <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties"/>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol name="3" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="round" k="joinstyle"/>
          <prop v="234,13,205,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.25" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="MarkerLine" pass="0" enabled="1">
          <prop v="4" k="average_angle_length"/>
          <prop v="3x:0,0,0,0,0,0" k="average_angle_map_unit_scale"/>
          <prop v="MM" k="average_angle_unit"/>
          <prop v="5" k="interval"/>
          <prop v="3x:0,0,0,0,0,0" k="interval_map_unit_scale"/>
          <prop v="MM" k="interval_unit"/>
          <prop v="0" k="offset"/>
          <prop v="0" k="offset_along_line"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_along_line_map_unit_scale"/>
          <prop v="MM" k="offset_along_line_unit"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="interval" k="placement"/>
          <prop v="0" k="ring_filter"/>
          <prop v="1" k="rotate"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
          <symbol name="@3@1" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
            <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
              <prop v="0" k="angle"/>
              <prop v="255,0,0,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="cross2" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="234,13,205,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties"/>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
      <symbol name="4" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop v="0" k="align_dash_pattern"/>
          <prop v="round" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MM" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="round" k="joinstyle"/>
          <prop v="255,135,253,255" k="line_color"/>
          <prop v="solid" k="line_style"/>
          <prop v="0.3" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style namedStyle="Regular" fieldName="'MULTI_'|| &quot;materiau&quot; ||'_'|| &quot;dimension&quot; " fontSizeMapUnitScale="3x:0,0,0,0,0,0" textColor="79,52,4,255" fontWeight="50" blendMode="0" multilineHeight="1" fontSize="8" isExpression="1" fontKerning="1" fontStrikeout="0" allowHtml="0" fontItalic="0" fontLetterSpacing="0" fontWordSpacing="0" fontSizeUnit="Point" capitalization="0" fontUnderline="0" textOpacity="1" fontFamily="Arial Black" textOrientation="horizontal" useSubstitutions="1" previewBkgrdColor="255,255,255,255">
        <text-buffer bufferJoinStyle="128" bufferSizeUnits="MM" bufferBlendMode="0" bufferSize="1" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferNoFill="1" bufferDraw="0" bufferColor="255,255,255,255"/>
        <text-mask maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskJoinStyle="128" maskOpacity="1" maskSizeUnits="MM" maskedSymbolLayers="" maskSize="1.5" maskType="0" maskEnabled="0"/>
        <background shapeRadiiUnit="MM" shapeBlendMode="0" shapeOffsetX="0" shapeDraw="0" shapeOffsetUnit="MM" shapeBorderWidthUnit="MM" shapeFillColor="255,255,255,255" shapeBorderColor="128,128,128,255" shapeSizeY="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSizeType="0" shapeSVGFile="" shapeOpacity="1" shapeOffsetY="0" shapeJoinStyle="64" shapeBorderWidth="0" shapeSizeX="0" shapeRotationType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeRadiiX="0" shapeSizeUnit="MM" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeRotation="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0">
          <symbol name="markerSymbol" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
            <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
              <prop v="0" k="angle"/>
              <prop v="133,182,111,255" k="color"/>
              <prop v="1" k="horizontal_anchor_point"/>
              <prop v="bevel" k="joinstyle"/>
              <prop v="circle" k="name"/>
              <prop v="0,0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="35,35,35,255" k="outline_color"/>
              <prop v="solid" k="outline_style"/>
              <prop v="0" k="outline_width"/>
              <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
              <prop v="MM" k="outline_width_unit"/>
              <prop v="diameter" k="scale_method"/>
              <prop v="2" k="size"/>
              <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
              <prop v="MM" k="size_unit"/>
              <prop v="1" k="vertical_anchor_point"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" name="name" type="QString"/>
                  <Option name="properties"/>
                  <Option value="collection" name="type" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowRadiusAlphaOnly="0" shadowOffsetGlobal="1" shadowRadius="1.5" shadowRadiusUnit="MM" shadowBlendMode="6" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowDraw="0" shadowColor="0,0,0,255" shadowOffsetUnit="MM" shadowOffsetAngle="135" shadowOffsetDist="1" shadowUnder="0" shadowOpacity="0.7" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
        </dd_properties>
        <substitutions>
          <replacement replace="AC" wholeWord="1" caseSensitive="0" match="asbestos"/>
          <replacement replace="Fe-noir" wholeWord="1" caseSensitive="0" match="blackIron"/>
          <replacement replace="ACI-noir" wholeWord="1" caseSensitive="0" match="blackSteel"/>
          <replacement replace="FTE" wholeWord="1" caseSensitive="0" match="castIron"/>
          <replacement replace="BET-comp" wholeWord="1" caseSensitive="0" match="compositeConcrete"/>
          <replacement replace="BET" wholeWord="1" caseSensitive="0" match="concrete"/>
          <replacement replace="PVC-C" wholeWord="1" caseSensitive="0" match="CPVC"/>
          <replacement replace="GALVA" wholeWord="1" caseSensitive="0" match="galvanizedSteel"/>
          <replacement replace="Maç" wholeWord="1" caseSensitive="0" match="masonry"/>
          <replacement replace="AUT" wholeWord="1" caseSensitive="0" match="other"/>
          <replacement replace="PRECONT" wholeWord="1" caseSensitive="0" match="prestressedReinforcedConcrete"/>
          <replacement replace="BET-Arm" wholeWord="1" caseSensitive="0" match="reinforcedConcrete"/>
          <replacement replace="ACI" wholeWord="1" caseSensitive="0" match="steel"/>
          <replacement replace="TC" wholeWord="1" caseSensitive="0" match="terracota"/>
          <replacement replace="BOIS" wholeWord="1" caseSensitive="0" match="wood"/>
        </substitutions>
      </text-style>
      <text-format leftDirectionSymbol="&lt;" formatNumbers="0" rightDirectionSymbol=">" wrapChar="" reverseDirectionSymbol="0" plussign="0" useMaxLineLengthForAutoWrap="1" placeDirectionSymbol="0" decimals="3" addDirectionSymbol="0" multilineAlign="0" autoWrapLength="0"/>
      <placement fitInPolygonOnly="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" maxCurvedCharAngleIn="25" overrunDistance="0" overrunDistanceUnit="MM" lineAnchorPercent="0.5" priority="5" repeatDistanceUnits="RenderMetersInMapUnits" offsetUnits="MM" geometryGeneratorEnabled="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" distMapUnitScale="3x:0,0,0,0,0,0" quadOffset="4" centroidWhole="0" distUnits="MapUnit" lineAnchorType="0" rotationAngle="0" repeatDistance="0" geometryGeneratorType="PointGeometry" centroidInside="0" placementFlags="12" maxCurvedCharAngleOut="-25" offsetType="0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" preserveRotation="1" geometryGenerator="" polygonPlacementFlags="2" layerType="LineGeometry" dist="0.5" xOffset="0" yOffset="0" placement="2" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0"/>
      <rendering mergeLines="0" obstacleFactor="1" limitNumLabels="0" minFeatureSize="0" scaleMin="0" maxNumLabels="2000" obstacle="1" scaleVisibility="0" displayAll="0" labelPerPart="0" obstacleType="1" drawLabels="1" fontMinPixelSize="3" fontMaxPixelSize="10000" zIndex="0" upsidedownLabels="0" scaleMax="0" fontLimitPixelSize="0"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" name="name" type="QString"/>
          <Option name="properties"/>
          <Option value="collection" name="type" type="QString"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" name="anchorPoint" type="QString"/>
          <Option name="ddProperties" type="Map">
            <Option value="" name="name" type="QString"/>
            <Option name="properties"/>
            <Option value="collection" name="type" type="QString"/>
          </Option>
          <Option value="false" name="drawToAllParts" type="bool"/>
          <Option value="0" name="enabled" type="QString"/>
          <Option value="point_on_exterior" name="labelAnchorPoint" type="QString"/>
          <Option value="&lt;symbol name=&quot;symbol&quot; clip_to_extent=&quot;1&quot; type=&quot;line&quot; alpha=&quot;1&quot; force_rhr=&quot;0&quot;>&lt;layer locked=&quot;0&quot; class=&quot;SimpleLine&quot; pass=&quot;0&quot; enabled=&quot;1&quot;>&lt;prop v=&quot;0&quot; k=&quot;align_dash_pattern&quot;/>&lt;prop v=&quot;square&quot; k=&quot;capstyle&quot;/>&lt;prop v=&quot;5;2&quot; k=&quot;customdash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;customdash_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;customdash_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;dash_pattern_offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;dash_pattern_offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;draw_inside_polygon&quot;/>&lt;prop v=&quot;bevel&quot; k=&quot;joinstyle&quot;/>&lt;prop v=&quot;60,60,60,255&quot; k=&quot;line_color&quot;/>&lt;prop v=&quot;solid&quot; k=&quot;line_style&quot;/>&lt;prop v=&quot;0.3&quot; k=&quot;line_width&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;line_width_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;offset&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;offset_map_unit_scale&quot;/>&lt;prop v=&quot;MM&quot; k=&quot;offset_unit&quot;/>&lt;prop v=&quot;0&quot; k=&quot;ring_filter&quot;/>&lt;prop v=&quot;0&quot; k=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;prop v=&quot;0&quot; k=&quot;use_custom_dash&quot;/>&lt;prop v=&quot;3x:0,0,0,0,0,0&quot; k=&quot;width_map_unit_scale&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; name=&quot;name&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; name=&quot;type&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol" type="QString"/>
          <Option value="0" name="minLength" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale" type="QString"/>
          <Option value="MM" name="minLengthUnit" type="QString"/>
          <Option value="0" name="offsetFromAnchor" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale" type="QString"/>
          <Option value="MM" name="offsetFromAnchorUnit" type="QString"/>
          <Option value="0" name="offsetFromLabel" type="double"/>
          <Option value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale" type="QString"/>
          <Option value="MM" name="offsetFromLabelUnit" type="QString"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" direction="0" opacity="1" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" penWidth="0" height="15" penAlpha="255" backgroundAlpha="255" enabled="0" scaleBasedVisibility="0" minScaleDenominator="0" barWidth="5" lineSizeType="MM" rotationOffset="270" spacing="5" sizeType="MM" scaleDependency="Area" spacingUnit="MM" width="15" maxScaleDenominator="1e+08" penColor="#000000" diagramOrientation="Up" spacingUnitScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" showAxis="1">
      <fontProperties description="Roboto,11,-1,5,25,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
      <axisSymbol>
        <symbol name="" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
          <layer locked="0" class="SimpleLine" pass="0" enabled="1">
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" dist="0" linePlacementFlags="18" priority="0" showAll="1" placement="2">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="1" geometryPrecision="0">
    <activeChecks type="StringList">
      <Option value="QgsIsValidCheck" type="QString"/>
    </activeChecks>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="identifiant" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionxy" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionz" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="xyschematique" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="visiblesurface" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sensible" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="statut" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="functional" name="Actif" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="projected" name="En projet" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underConstruction" name="En cours de construction/modification" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="decommissioned" name="Déclassé" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validede" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validejusque" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="positionverticale" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="onGroundSurface" name="au niveau du sol" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="suspendedOrElevated" name="suspendu ou surelevé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underground" name="sous le sol" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="miseajour" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dimension" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="caracteristiques" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="typeelement" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="collecte" name="collecte" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="distribution" name="distribution" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="prive" name="privé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="transport" name="transport" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dispositifprotection" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="net" name="filet" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="tape" name="ruban" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="concretePaving" name="pavage en beton" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="unknown" name="inconnu" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="profondeurminreg" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="profondeurminnonreg" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="materiau" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="ABS" name="ABS" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="asbestos" name="amiante" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="blackIron" name="fer noir" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="blackSteel" name="acier noir" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="castIron" name="fonte" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="clay" name="argile" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="compositeConcrete" name="béton composite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="concrete" name="béton" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="CPVC" name="PVCC" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="FRP" name="plastique renforcé de fibres (FRP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="galvanizedSteel" name="métal galvanisé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="masonry" name="maçonnerie" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="other" name="autre" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PB" name="polybutylène (PB)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PE" name="polyéthylène (PE)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PEX" name="polyéthylène réticulé à haute densité (PEX)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PP" name="polypropylène (PP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="prestressedReinforcedConcrete" name="béton armé précontraint" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PVC" name="PVC" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="reinforcedConcrete" name="béton renforcé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="RPMP" name="mortier renforcé de polymères(RPMP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="steel" name="acier" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="terracota" name="terre cuite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="wood" name="bois" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="hierarchie" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="reseau" name="Réseau" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="branchement" name="Branchement" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="commentaire" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="true" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="hauteurminreg" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="exemptionic" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="idouvrage" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="fid" index="0"/>
    <alias name="" field="identifiant" index="1"/>
    <alias name="" field="code" index="2"/>
    <alias name="" field="precisionxy" index="3"/>
    <alias name="" field="precisionz" index="4"/>
    <alias name="" field="xyschematique" index="5"/>
    <alias name="" field="visiblesurface" index="6"/>
    <alias name="" field="sensible" index="7"/>
    <alias name="" field="statut" index="8"/>
    <alias name="" field="validede" index="9"/>
    <alias name="" field="validejusque" index="10"/>
    <alias name="" field="positionverticale" index="11"/>
    <alias name="" field="miseajour" index="12"/>
    <alias name="" field="dimension" index="13"/>
    <alias name="" field="caracteristiques" index="14"/>
    <alias name="" field="typeelement" index="15"/>
    <alias name="" field="dispositifprotection" index="16"/>
    <alias name="" field="profondeurminreg" index="17"/>
    <alias name="" field="profondeurminnonreg" index="18"/>
    <alias name="" field="materiau" index="19"/>
    <alias name="" field="hierarchie" index="20"/>
    <alias name="" field="commentaire" index="21"/>
    <alias name="hauteru (si aérien) (en m)" field="hauteurminreg" index="22"/>
    <alias name="" field="exemptionic" index="23"/>
    <alias name="" field="idouvrage" index="24"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="identifiant" expression="" applyOnUpdate="0"/>
    <default field="code" expression="" applyOnUpdate="0"/>
    <default field="precisionxy" expression="" applyOnUpdate="0"/>
    <default field="precisionz" expression="" applyOnUpdate="0"/>
    <default field="xyschematique" expression="" applyOnUpdate="0"/>
    <default field="visiblesurface" expression="" applyOnUpdate="0"/>
    <default field="sensible" expression="0" applyOnUpdate="0"/>
    <default field="statut" expression="'functional'" applyOnUpdate="0"/>
    <default field="validede" expression="" applyOnUpdate="0"/>
    <default field="validejusque" expression="" applyOnUpdate="0"/>
    <default field="positionverticale" expression="'underground'" applyOnUpdate="0"/>
    <default field="miseajour" expression="" applyOnUpdate="0"/>
    <default field="dimension" expression="" applyOnUpdate="0"/>
    <default field="caracteristiques" expression="" applyOnUpdate="0"/>
    <default field="typeelement" expression="" applyOnUpdate="0"/>
    <default field="dispositifprotection" expression="" applyOnUpdate="0"/>
    <default field="profondeurminreg" expression="" applyOnUpdate="0"/>
    <default field="profondeurminnonreg" expression="" applyOnUpdate="0"/>
    <default field="materiau" expression="" applyOnUpdate="0"/>
    <default field="hierarchie" expression="'reseau'" applyOnUpdate="0"/>
    <default field="commentaire" expression="" applyOnUpdate="0"/>
    <default field="hauteurminreg" expression="" applyOnUpdate="0"/>
    <default field="exemptionic" expression="" applyOnUpdate="0"/>
    <default field="idouvrage" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="fid" constraints="3" unique_strength="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="identifiant" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="code" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionxy" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionz" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="xyschematique" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="visiblesurface" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="sensible" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="statut" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validede" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validejusque" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="positionverticale" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="miseajour" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="dimension" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="caracteristiques" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="typeelement" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="dispositifprotection" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="profondeurminreg" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="profondeurminnonreg" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="materiau" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="hierarchie" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="commentaire" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="hauteurminreg" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="exemptionic" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="idouvrage" constraints="0" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="identifiant" desc="" exp=""/>
    <constraint field="code" desc="" exp=""/>
    <constraint field="precisionxy" desc="" exp=""/>
    <constraint field="precisionz" desc="" exp=""/>
    <constraint field="xyschematique" desc="" exp=""/>
    <constraint field="visiblesurface" desc="" exp=""/>
    <constraint field="sensible" desc="" exp=""/>
    <constraint field="statut" desc="" exp=""/>
    <constraint field="validede" desc="" exp=""/>
    <constraint field="validejusque" desc="" exp=""/>
    <constraint field="positionverticale" desc="" exp=""/>
    <constraint field="miseajour" desc="" exp=""/>
    <constraint field="dimension" desc="" exp=""/>
    <constraint field="caracteristiques" desc="" exp=""/>
    <constraint field="typeelement" desc="" exp=""/>
    <constraint field="dispositifprotection" desc="" exp=""/>
    <constraint field="profondeurminreg" desc="" exp=""/>
    <constraint field="profondeurminnonreg" desc="" exp=""/>
    <constraint field="materiau" desc="" exp=""/>
    <constraint field="hierarchie" desc="" exp=""/>
    <constraint field="commentaire" desc="" exp=""/>
    <constraint field="hauteurminreg" desc="" exp=""/>
    <constraint field="exemptionic" desc="" exp=""/>
    <constraint field="idouvrage" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="fid" hidden="0" type="field" width="-1"/>
      <column name="identifiant" hidden="0" type="field" width="-1"/>
      <column name="code" hidden="0" type="field" width="-1"/>
      <column name="precisionxy" hidden="0" type="field" width="-1"/>
      <column name="precisionz" hidden="0" type="field" width="-1"/>
      <column name="xyschematique" hidden="0" type="field" width="-1"/>
      <column name="visiblesurface" hidden="0" type="field" width="-1"/>
      <column name="sensible" hidden="0" type="field" width="-1"/>
      <column name="statut" hidden="0" type="field" width="-1"/>
      <column name="validede" hidden="0" type="field" width="-1"/>
      <column name="validejusque" hidden="0" type="field" width="-1"/>
      <column name="positionverticale" hidden="0" type="field" width="-1"/>
      <column name="miseajour" hidden="0" type="field" width="-1"/>
      <column name="dimension" hidden="0" type="field" width="-1"/>
      <column name="caracteristiques" hidden="0" type="field" width="-1"/>
      <column name="typeelement" hidden="0" type="field" width="-1"/>
      <column name="dispositifprotection" hidden="0" type="field" width="-1"/>
      <column name="profondeurminreg" hidden="0" type="field" width="-1"/>
      <column name="profondeurminnonreg" hidden="0" type="field" width="-1"/>
      <column name="materiau" hidden="0" type="field" width="-1"/>
      <column name="hierarchie" hidden="0" type="field" width="-1"/>
      <column name="commentaire" hidden="0" type="field" width="-1"/>
      <column name="hauteurminreg" hidden="0" type="field" width="-1"/>
      <column name="exemptionic" hidden="0" type="field" width="-1"/>
      <column name="idouvrage" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="statut" showLabel="1" index="8"/>
    <attributeEditorContainer name="tronçon" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="typeelement" showLabel="1" index="15"/>
      <attributeEditorField name="hierarchie" showLabel="1" index="20"/>
      <attributeEditorField name="materiau" showLabel="1" index="19"/>
      <attributeEditorField name="dispositifprotection" showLabel="1" index="16"/>
      <attributeEditorField name="profondeurminreg" showLabel="1" index="17"/>
      <attributeEditorField name="profondeurminnonreg" showLabel="1" index="18"/>
      <attributeEditorField name="hauteurminreg" showLabel="1" index="22"/>
      <attributeEditorField name="exemptionic" showLabel="1" index="23"/>
    </attributeEditorContainer>
    <attributeEditorContainer name="ouvrage" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="dimension" showLabel="1" index="13"/>
      <attributeEditorField name="caracteristiques" showLabel="1" index="14"/>
      <attributeEditorField name="validede" showLabel="1" index="9"/>
      <attributeEditorField name="validejusque" showLabel="1" index="10"/>
      <attributeEditorField name="positionverticale" showLabel="1" index="11"/>
    </attributeEditorContainer>
    <attributeEditorContainer name="Element réseau" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="identifiant" showLabel="1" index="1"/>
      <attributeEditorField name="code" showLabel="1" index="2"/>
      <attributeEditorField name="precisionxy" showLabel="1" index="3"/>
      <attributeEditorField name="precisionz" showLabel="1" index="4"/>
      <attributeEditorField name="xyschematique" showLabel="1" index="5"/>
      <attributeEditorField name="visiblesurface" showLabel="1" index="6"/>
      <attributeEditorField name="sensible" showLabel="1" index="7"/>
    </attributeEditorContainer>
    <attributeEditorField name="commentaire" showLabel="1" index="21"/>
    <attributeEditorField name="miseajour" showLabel="1" index="12"/>
  </attributeEditorForm>
  <editable>
    <field name="caracteristiques" editable="1"/>
    <field name="classetension" editable="1"/>
    <field name="code" editable="1"/>
    <field name="commentaire" editable="1"/>
    <field name="dimension" editable="1"/>
    <field name="dispositifprotection" editable="1"/>
    <field name="ecoulement" editable="1"/>
    <field name="exemptionic" editable="1"/>
    <field name="fid" editable="0"/>
    <field name="fonctioncable" editable="1"/>
    <field name="hauteurminreg" editable="1"/>
    <field name="hierarchie" editable="1"/>
    <field name="identifiant" editable="1"/>
    <field name="idouvrage" editable="1"/>
    <field name="materiau" editable="1"/>
    <field name="miseajour" editable="0"/>
    <field name="positionverticale" editable="1"/>
    <field name="precisionxy" editable="1"/>
    <field name="precisionz" editable="1"/>
    <field name="profondeurminnonreg" editable="1"/>
    <field name="profondeurminreg" editable="1"/>
    <field name="regime" editable="1"/>
    <field name="sensible" editable="1"/>
    <field name="statut" editable="1"/>
    <field name="technocable" editable="1"/>
    <field name="typeassainissement" editable="1"/>
    <field name="typeelement" editable="1"/>
    <field name="validede" editable="1"/>
    <field name="validejusque" editable="1"/>
    <field name="visiblesurface" editable="1"/>
    <field name="xyschematique" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="caracteristiques" labelOnTop="0"/>
    <field name="classetension" labelOnTop="0"/>
    <field name="code" labelOnTop="0"/>
    <field name="commentaire" labelOnTop="0"/>
    <field name="dimension" labelOnTop="0"/>
    <field name="dispositifprotection" labelOnTop="0"/>
    <field name="ecoulement" labelOnTop="0"/>
    <field name="exemptionic" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="fonctioncable" labelOnTop="0"/>
    <field name="hauteurminreg" labelOnTop="0"/>
    <field name="hierarchie" labelOnTop="0"/>
    <field name="identifiant" labelOnTop="0"/>
    <field name="idouvrage" labelOnTop="0"/>
    <field name="materiau" labelOnTop="0"/>
    <field name="miseajour" labelOnTop="0"/>
    <field name="positionverticale" labelOnTop="0"/>
    <field name="precisionxy" labelOnTop="0"/>
    <field name="precisionz" labelOnTop="0"/>
    <field name="profondeurminnonreg" labelOnTop="0"/>
    <field name="profondeurminreg" labelOnTop="0"/>
    <field name="regime" labelOnTop="0"/>
    <field name="sensible" labelOnTop="0"/>
    <field name="statut" labelOnTop="0"/>
    <field name="technocable" labelOnTop="0"/>
    <field name="typeassainissement" labelOnTop="0"/>
    <field name="typeelement" labelOnTop="0"/>
    <field name="validede" labelOnTop="0"/>
    <field name="validejusque" labelOnTop="0"/>
    <field name="visiblesurface" labelOnTop="0"/>
    <field name="xyschematique" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"fid"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
