<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="1" simplifyMaxScale="1" simplifyDrawingTol="1" maxScale="0" simplifyLocal="1" readOnly="0" labelsEnabled="0" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" version="3.16.16-Hannover" minScale="100000000">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal accumulate="0" startExpression="" endExpression="" startField="" durationField="" mode="0" enabled="0" endField="" durationUnit="min" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" type="singleSymbol" forceraster="0" enableorderby="0">
    <symbols>
      <symbol name="0" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop v="0" k="align_dash_pattern"/>
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MapUnit" k="customdash_unit"/>
          <prop v="0" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="dot" k="line_style"/>
          <prop v="0.7" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MapUnit" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="offset" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="(&quot;largeur&quot;)/2+0.02" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
                <Option name="outlineWidth" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option value="" name="val" type="QString"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="SimpleLine" pass="0" enabled="1">
          <prop v="0" k="align_dash_pattern"/>
          <prop v="square" k="capstyle"/>
          <prop v="5;2" k="customdash"/>
          <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
          <prop v="MapUnit" k="customdash_unit"/>
          <prop v="1" k="dash_pattern_offset"/>
          <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
          <prop v="MM" k="dash_pattern_offset_unit"/>
          <prop v="0" k="draw_inside_polygon"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0,0,255" k="line_color"/>
          <prop v="dot" k="line_style"/>
          <prop v="0.7" k="line_width"/>
          <prop v="MM" k="line_width_unit"/>
          <prop v="0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MapUnit" k="offset_unit"/>
          <prop v="0" k="ring_filter"/>
          <prop v="0" k="tweak_dash_pattern_on_corners"/>
          <prop v="0" k="use_custom_dash"/>
          <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="offset" type="Map">
                  <Option value="true" name="active" type="bool"/>
                  <Option value="-(&quot;largeur&quot;)/2+0.02" name="expression" type="QString"/>
                  <Option value="3" name="type" type="int"/>
                </Option>
                <Option name="outlineWidth" type="Map">
                  <Option value="false" name="active" type="bool"/>
                  <Option value="1" name="type" type="int"/>
                  <Option value="" name="val" type="QString"/>
                </Option>
              </Option>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" direction="0" opacity="1" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" penWidth="0" height="15" penAlpha="255" backgroundAlpha="255" enabled="0" scaleBasedVisibility="0" minScaleDenominator="0" barWidth="5" lineSizeType="MM" rotationOffset="270" spacing="5" sizeType="MM" scaleDependency="Area" spacingUnit="MM" width="15" maxScaleDenominator="1e+08" penColor="#000000" diagramOrientation="Up" spacingUnitScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" showAxis="1">
      <fontProperties description="Roboto,11,-1,5,25,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
      <axisSymbol>
        <symbol name="" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
          <layer locked="0" class="SimpleLine" pass="0" enabled="1">
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" dist="0" linePlacementFlags="18" priority="0" showAll="1" placement="2">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="identifiant" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionxy" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionz" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="xyschematique" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="visiblesurface" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sensible" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="statut" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="functional" name="Actif" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="projected" name="En projet" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underConstruction" name="En cours de construction/modification" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="decommissioned" name="Déclassé" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validede" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validejusque" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="positionverticale" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="onGroundSurface" name="au niveau du sol" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="suspendedOrElevated" name="suspendu ou surelevé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underground" name="sous le sol" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="miseajour" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dimension" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="caracteristiques" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="typeelement" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="collecte" name="collecte" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="distribution" name="distribution" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="prive" name="privé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="transport" name="transport" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dispositifprotection" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="net" name="filet" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="tape" name="ruban" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="concretePaving" name="pavage en beton" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="unknown" name="inconnu" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="profondeurminreg" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="profondeurminnonreg" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="materiau" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="ABS" name="ABS" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="asbestos" name="amiante" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="blackIron" name="fer noir" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="blackSteel" name="acier noir" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="castIron" name="fonte" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="clay" name="argile" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="compositeConcrete" name="béton composite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="concrete" name="béton" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="CPVC" name="PVCC" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="FRP" name="plastique renforcé de fibres (FRP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="galvanizedSteel" name="métal galvanisé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="masonry" name="maçonnerie" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="other" name="autre" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PB" name="polybutylène (PB)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PE" name="polyéthylène (PE)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PEX" name="polyéthylène réticulé à haute densité (PEX)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PP" name="polypropylène (PP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="prestressedReinforcedConcrete" name="béton armé précontraint" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PVC" name="PVC" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="reinforcedConcrete" name="béton renforcé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="RPMP" name="mortier renforcé de polymères(RPMP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="steel" name="acier" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="terracota" name="terre cuite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="wood" name="bois" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="hierarchie" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="reseau" name="Réseau" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="branchement" name="Branchement" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="commentaire" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="true" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="hauteurminreg" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="exemptionic" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="nombreouvrages" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="false" name="AllowNull" type="bool"/>
            <Option value="30" name="Max" type="int"/>
            <Option value="1" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="largeur" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="hauteur" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="idouvrage" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="fid" index="0"/>
    <alias name="" field="identifiant" index="1"/>
    <alias name="" field="code" index="2"/>
    <alias name="" field="precisionxy" index="3"/>
    <alias name="" field="precisionz" index="4"/>
    <alias name="" field="xyschematique" index="5"/>
    <alias name="" field="visiblesurface" index="6"/>
    <alias name="" field="sensible" index="7"/>
    <alias name="" field="statut" index="8"/>
    <alias name="" field="validede" index="9"/>
    <alias name="" field="validejusque" index="10"/>
    <alias name="" field="positionverticale" index="11"/>
    <alias name="" field="miseajour" index="12"/>
    <alias name="" field="dimension" index="13"/>
    <alias name="" field="caracteristiques" index="14"/>
    <alias name="" field="typeelement" index="15"/>
    <alias name="" field="dispositifprotection" index="16"/>
    <alias name="" field="profondeurminreg" index="17"/>
    <alias name="" field="profondeurminnonreg" index="18"/>
    <alias name="" field="materiau" index="19"/>
    <alias name="" field="hierarchie" index="20"/>
    <alias name="" field="commentaire" index="21"/>
    <alias name="" field="hauteurminreg" index="22"/>
    <alias name="" field="exemptionic" index="23"/>
    <alias name="" field="nombreouvrages" index="24"/>
    <alias name="largeur (en m)" field="largeur" index="25"/>
    <alias name="hauteur (en m)" field="hauteur" index="26"/>
    <alias name="" field="idouvrage" index="27"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="identifiant" expression="" applyOnUpdate="0"/>
    <default field="code" expression="" applyOnUpdate="0"/>
    <default field="precisionxy" expression="'C'" applyOnUpdate="0"/>
    <default field="precisionz" expression="'C'" applyOnUpdate="0"/>
    <default field="xyschematique" expression="" applyOnUpdate="0"/>
    <default field="visiblesurface" expression="" applyOnUpdate="0"/>
    <default field="sensible" expression="" applyOnUpdate="0"/>
    <default field="statut" expression="'functional'" applyOnUpdate="0"/>
    <default field="validede" expression="" applyOnUpdate="0"/>
    <default field="validejusque" expression="" applyOnUpdate="0"/>
    <default field="positionverticale" expression="'underground'" applyOnUpdate="0"/>
    <default field="miseajour" expression="" applyOnUpdate="0"/>
    <default field="dimension" expression="" applyOnUpdate="0"/>
    <default field="caracteristiques" expression="" applyOnUpdate="0"/>
    <default field="typeelement" expression="" applyOnUpdate="0"/>
    <default field="dispositifprotection" expression="" applyOnUpdate="0"/>
    <default field="profondeurminreg" expression="" applyOnUpdate="0"/>
    <default field="profondeurminnonreg" expression="" applyOnUpdate="0"/>
    <default field="materiau" expression="" applyOnUpdate="0"/>
    <default field="hierarchie" expression="" applyOnUpdate="0"/>
    <default field="commentaire" expression="" applyOnUpdate="0"/>
    <default field="hauteurminreg" expression="" applyOnUpdate="0"/>
    <default field="exemptionic" expression="" applyOnUpdate="0"/>
    <default field="nombreouvrages" expression="1" applyOnUpdate="0"/>
    <default field="largeur" expression="0.5" applyOnUpdate="0"/>
    <default field="hauteur" expression="" applyOnUpdate="0"/>
    <default field="idouvrage" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="fid" constraints="3" unique_strength="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="identifiant" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="code" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionxy" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionz" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="xyschematique" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="visiblesurface" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="sensible" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="statut" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validede" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validejusque" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="positionverticale" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="miseajour" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="dimension" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="caracteristiques" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="typeelement" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="dispositifprotection" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="profondeurminreg" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="profondeurminnonreg" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="materiau" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="hierarchie" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="commentaire" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="hauteurminreg" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="exemptionic" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="nombreouvrages" constraints="1" unique_strength="0" notnull_strength="2"/>
    <constraint exp_strength="0" field="largeur" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="hauteur" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="idouvrage" constraints="0" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="identifiant" desc="" exp=""/>
    <constraint field="code" desc="" exp=""/>
    <constraint field="precisionxy" desc="" exp=""/>
    <constraint field="precisionz" desc="" exp=""/>
    <constraint field="xyschematique" desc="" exp=""/>
    <constraint field="visiblesurface" desc="" exp=""/>
    <constraint field="sensible" desc="" exp=""/>
    <constraint field="statut" desc="" exp=""/>
    <constraint field="validede" desc="" exp=""/>
    <constraint field="validejusque" desc="" exp=""/>
    <constraint field="positionverticale" desc="" exp=""/>
    <constraint field="miseajour" desc="" exp=""/>
    <constraint field="dimension" desc="" exp=""/>
    <constraint field="caracteristiques" desc="" exp=""/>
    <constraint field="typeelement" desc="" exp=""/>
    <constraint field="dispositifprotection" desc="" exp=""/>
    <constraint field="profondeurminreg" desc="" exp=""/>
    <constraint field="profondeurminnonreg" desc="" exp=""/>
    <constraint field="materiau" desc="" exp=""/>
    <constraint field="hierarchie" desc="" exp=""/>
    <constraint field="commentaire" desc="" exp=""/>
    <constraint field="hauteurminreg" desc="" exp=""/>
    <constraint field="exemptionic" desc="" exp=""/>
    <constraint field="nombreouvrages" desc="" exp=""/>
    <constraint field="largeur" desc="" exp=""/>
    <constraint field="hauteur" desc="" exp=""/>
    <constraint field="idouvrage" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="fid" hidden="0" type="field" width="-1"/>
      <column name="identifiant" hidden="0" type="field" width="-1"/>
      <column name="code" hidden="0" type="field" width="-1"/>
      <column name="precisionxy" hidden="0" type="field" width="-1"/>
      <column name="precisionz" hidden="0" type="field" width="-1"/>
      <column name="xyschematique" hidden="0" type="field" width="-1"/>
      <column name="visiblesurface" hidden="0" type="field" width="-1"/>
      <column name="sensible" hidden="0" type="field" width="-1"/>
      <column name="statut" hidden="0" type="field" width="-1"/>
      <column name="validede" hidden="0" type="field" width="-1"/>
      <column name="validejusque" hidden="0" type="field" width="-1"/>
      <column name="positionverticale" hidden="0" type="field" width="-1"/>
      <column name="miseajour" hidden="0" type="field" width="-1"/>
      <column name="dimension" hidden="0" type="field" width="-1"/>
      <column name="caracteristiques" hidden="0" type="field" width="-1"/>
      <column name="typeelement" hidden="0" type="field" width="-1"/>
      <column name="dispositifprotection" hidden="0" type="field" width="-1"/>
      <column name="profondeurminreg" hidden="0" type="field" width="-1"/>
      <column name="profondeurminnonreg" hidden="0" type="field" width="-1"/>
      <column name="materiau" hidden="0" type="field" width="-1"/>
      <column name="hierarchie" hidden="0" type="field" width="-1"/>
      <column name="commentaire" hidden="0" type="field" width="-1"/>
      <column name="hauteurminreg" hidden="0" type="field" width="-1"/>
      <column name="exemptionic" hidden="0" type="field" width="-1"/>
      <column name="nombreouvrages" hidden="0" type="field" width="-1"/>
      <column name="idouvrage" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
      <column name="largeur" hidden="0" type="field" width="-1"/>
      <column name="hauteur" hidden="0" type="field" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="nombreouvrages" showLabel="1" index="24"/>
    <attributeEditorField name="largeur" showLabel="1" index="25"/>
    <attributeEditorField name="hauteur" showLabel="1" index="26"/>
    <attributeEditorField name="statut" showLabel="1" index="8"/>
    <attributeEditorContainer name="tronçon" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="typeelement" showLabel="1" index="15"/>
      <attributeEditorField name="hierarchie" showLabel="1" index="20"/>
      <attributeEditorField name="materiau" showLabel="1" index="19"/>
      <attributeEditorField name="dispositifprotection" showLabel="1" index="16"/>
      <attributeEditorField name="profondeurminreg" showLabel="1" index="17"/>
      <attributeEditorField name="profondeurminnonreg" showLabel="1" index="18"/>
      <attributeEditorField name="hauteurminreg" showLabel="1" index="22"/>
      <attributeEditorField name="exemptionic" showLabel="1" index="23"/>
    </attributeEditorContainer>
    <attributeEditorContainer name="ouvrage" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="dimension" showLabel="1" index="13"/>
      <attributeEditorField name="caracteristiques" showLabel="1" index="14"/>
      <attributeEditorField name="validede" showLabel="1" index="9"/>
      <attributeEditorField name="validejusque" showLabel="1" index="10"/>
      <attributeEditorField name="positionverticale" showLabel="1" index="11"/>
    </attributeEditorContainer>
    <attributeEditorContainer name="Element réseau" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="identifiant" showLabel="1" index="1"/>
      <attributeEditorField name="code" showLabel="1" index="2"/>
      <attributeEditorField name="precisionxy" showLabel="1" index="3"/>
      <attributeEditorField name="precisionz" showLabel="1" index="4"/>
      <attributeEditorField name="xyschematique" showLabel="1" index="5"/>
      <attributeEditorField name="visiblesurface" showLabel="1" index="6"/>
      <attributeEditorField name="sensible" showLabel="1" index="7"/>
    </attributeEditorContainer>
    <attributeEditorField name="commentaire" showLabel="1" index="21"/>
    <attributeEditorField name="miseajour" showLabel="1" index="12"/>
  </attributeEditorForm>
  <editable>
    <field name="caracteristiques" editable="1"/>
    <field name="code" editable="1"/>
    <field name="commentaire" editable="1"/>
    <field name="dimension" editable="1"/>
    <field name="dispositifprotection" editable="1"/>
    <field name="ecoulement" editable="1"/>
    <field name="exemptionic" editable="1"/>
    <field name="fid" editable="0"/>
    <field name="hauteur" editable="1"/>
    <field name="hauteurminreg" editable="1"/>
    <field name="hierarchie" editable="1"/>
    <field name="identifiant" editable="1"/>
    <field name="idouvrage" editable="1"/>
    <field name="largeur" editable="1"/>
    <field name="materiau" editable="1"/>
    <field name="miseajour" editable="0"/>
    <field name="nombreouvrages" editable="1"/>
    <field name="positionverticale" editable="1"/>
    <field name="precisionxy" editable="1"/>
    <field name="precisionz" editable="1"/>
    <field name="profondeurminnonreg" editable="1"/>
    <field name="profondeurminreg" editable="1"/>
    <field name="sensible" editable="1"/>
    <field name="statut" editable="1"/>
    <field name="typeassainissement" editable="1"/>
    <field name="typeelement" editable="1"/>
    <field name="validede" editable="1"/>
    <field name="validejusque" editable="1"/>
    <field name="visiblesurface" editable="1"/>
    <field name="xyschematique" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="caracteristiques" labelOnTop="0"/>
    <field name="code" labelOnTop="0"/>
    <field name="commentaire" labelOnTop="0"/>
    <field name="dimension" labelOnTop="0"/>
    <field name="dispositifprotection" labelOnTop="0"/>
    <field name="ecoulement" labelOnTop="0"/>
    <field name="exemptionic" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="hauteur" labelOnTop="0"/>
    <field name="hauteurminreg" labelOnTop="0"/>
    <field name="hierarchie" labelOnTop="0"/>
    <field name="identifiant" labelOnTop="0"/>
    <field name="idouvrage" labelOnTop="0"/>
    <field name="largeur" labelOnTop="0"/>
    <field name="materiau" labelOnTop="0"/>
    <field name="miseajour" labelOnTop="0"/>
    <field name="nombreouvrages" labelOnTop="0"/>
    <field name="positionverticale" labelOnTop="0"/>
    <field name="precisionxy" labelOnTop="0"/>
    <field name="precisionz" labelOnTop="0"/>
    <field name="profondeurminnonreg" labelOnTop="0"/>
    <field name="profondeurminreg" labelOnTop="0"/>
    <field name="sensible" labelOnTop="0"/>
    <field name="statut" labelOnTop="0"/>
    <field name="typeassainissement" labelOnTop="0"/>
    <field name="typeelement" labelOnTop="0"/>
    <field name="validede" labelOnTop="0"/>
    <field name="validejusque" labelOnTop="0"/>
    <field name="visiblesurface" labelOnTop="0"/>
    <field name="xyschematique" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"nombreouvrages"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
