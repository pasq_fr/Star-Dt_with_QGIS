<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" readOnly="0" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" version="3.16.16-Hannover" minScale="1e+08">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal accumulate="0" startExpression="" endExpression="" startField="" durationField="" mode="0" enabled="0" endField="" durationUnit="min" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>"nom"</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nom" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="theme" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="ELEC" name="Electricité" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="ELECECL" name="Eclairage public" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="ELECSLT" name="Signalisation électrique tricolore bassetension" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="ELECTRD" name="Electricité transport/distribution" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="GAZ" name="Gaz" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="CHIM" name="Produits chimiques" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="AEP" name="Eau potable" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="ASS" name="Assainissement et pluvial" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="ASSEP" name="Eaux pluviales" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="ASSEU" name="Eaux usées" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="CHAU" name="Chauffage et climatisation" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="COM" name="Télécom et signalisation lumineuse tricolore" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="DECH" name="Déchets" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="AUTREAU" name="Incendie, Irrigation, Eau brute, Eau salée, Eau non chlorée" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PINS" name="Ouvrage de protection inondation Submersion" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="MULT" name="Multi réseaux" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="INC" name="Non défini" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="profondeurstandard" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="mention" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="true" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="responsable" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="fk_elementreseau" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="fid" index="0"/>
    <alias name="" field="nom" index="1"/>
    <alias name="" field="theme" index="2"/>
    <alias name="" field="profondeurstandard" index="3"/>
    <alias name="" field="mention" index="4"/>
    <alias name="" field="responsable" index="5"/>
    <alias name="" field="fk_elementreseau" index="6"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="nom" expression="" applyOnUpdate="0"/>
    <default field="theme" expression="" applyOnUpdate="0"/>
    <default field="profondeurstandard" expression="" applyOnUpdate="0"/>
    <default field="mention" expression="" applyOnUpdate="0"/>
    <default field="responsable" expression="" applyOnUpdate="0"/>
    <default field="fk_elementreseau" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="fid" constraints="3" unique_strength="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="nom" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="theme" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="profondeurstandard" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="mention" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="responsable" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="fk_elementreseau" constraints="0" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="nom" desc="" exp=""/>
    <constraint field="theme" desc="" exp=""/>
    <constraint field="profondeurstandard" desc="" exp=""/>
    <constraint field="mention" desc="" exp=""/>
    <constraint field="responsable" desc="" exp=""/>
    <constraint field="fk_elementreseau" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="fid" hidden="0" type="field" width="-1"/>
      <column name="nom" hidden="0" type="field" width="-1"/>
      <column name="theme" hidden="0" type="field" width="-1"/>
      <column name="profondeurstandard" hidden="0" type="field" width="161"/>
      <column name="mention" hidden="0" type="field" width="-1"/>
      <column name="responsable" hidden="0" type="field" width="-1"/>
      <column name="fk_elementreseau" hidden="0" type="field" width="164"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="fid" editable="0"/>
    <field name="fk_elementreseau" editable="1"/>
    <field name="mention" editable="1"/>
    <field name="nom" editable="1"/>
    <field name="profondeurstandard" editable="1"/>
    <field name="responsable" editable="1"/>
    <field name="theme" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="fid" labelOnTop="0"/>
    <field name="fk_elementreseau" labelOnTop="0"/>
    <field name="mention" labelOnTop="0"/>
    <field name="nom" labelOnTop="0"/>
    <field name="profondeurstandard" labelOnTop="0"/>
    <field name="responsable" labelOnTop="0"/>
    <field name="theme" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"nom"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>4</layerGeometryType>
</qgis>
