<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyDrawingHints="0" simplifyMaxScale="1" simplifyDrawingTol="1" maxScale="0" simplifyLocal="1" readOnly="0" labelsEnabled="0" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" version="3.16.16-Hannover" minScale="100000000">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <temporal accumulate="0" startExpression="" endExpression="" startField="" durationField="" mode="0" enabled="0" endField="" durationUnit="min" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" type="RuleRenderer" forceraster="0" enableorderby="0">
    <rules key="{33ecd03b-6d89-424c-99b0-3d5b89ba8e88}">
      <rule label="déclassé" filter=" &quot;statut&quot; = 'decommisionned'" symbol="0" key="{ef276c96-b672-4072-ae25-f90cdba7fee0}"/>
      <rule symbol="1" key="{ce0bc09b-94df-4ade-a5f3-ecf36c144851}"/>
    </rules>
    <symbols>
      <symbol name="0" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="255,255,255,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="SvgMarker" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgaWQ9InN2ZzgiCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDI2LjQ1ODMzMjcyNDUgMjYuNDU4MzM0MDgxIgogICBoZWlnaHQ9IjEwMCIKICAgd2lkdGg9IjEwMCI+CiAgPGRlZnMKICAgICBpZD0iZGVmczIiIC8+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhNSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0yNzAuNTQxNjQ5ODgyKSIKICAgICBpZD0ibGF5ZXIxIj4KICAgIDxjaXJjbGUKICAgICAgIHI9IjEyLjcyOTE2Njk4NDYiCiAgICAgICBjeT0iMjgzLjc3MDgxMjk4OCIKICAgICAgIGN4PSIxMy4yMjkxNjY5ODQ2IgogICAgICAgaWQ9InBhdGg0NTE4IgogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxwYXRoCiAgICAgICBkPSJNIDEwLjQ1MTA0MTY2NjYsMjgwLjcxNzkzMTk0NyBIIDE2LjI3MTg3NSBtIC04LjE0OTE2NjY2NjgsNS42OTg3MTc5MzUgOC43MzEyNSwtMy43MDQxNjY2NTcgSCA5Ljg2ODk1ODMzMzE3IGwgOC40NDAyMDgzMzM0MywzLjcwNDE2NjY1NyBtIC0xMC4xODY0NTgzMzM0LDAgSCAxOC4zMDkxNjY2NjY2IE0gMTYuMjcxODc1LDI4MC43MTc5MzE5NDcgMjAuNjM3NSwyNzYuNDQzODkzNDk2IEggNi4wODU0MTY2NjY1NyBsIDQuMzY1NjI1MDAwMTMsNC4yNzQwMzg0NTEgbSAyLjkxMDQxNjY2NjUsLTcuMTIzMzk4MDQgLTcuMjc2MDQxNjY2NjMsMTguNTIwODMzOTEgTCAxMS45MDYyNSwyODYuNDE2NjQ5ODgyIGggMi45MTA0MTY2NjY2IEwgMjAuNjM3NSwyOTIuMTE1MzY3ODE3IFoiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjY5OTk5OTk4ODE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIGlkPSJwYXRoNDU2NCIgLz4KICA8L2c+Cjwvc3ZnPgo=" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="cross2" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4.4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" clip_to_extent="1" type="marker" alpha="1" force_rhr="0">
        <layer locked="0" class="SimpleMarker" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="255,255,255,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer locked="0" class="SvgMarker" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="0" k="fixedAspectRatio"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="base64:PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgaWQ9InN2ZzgiCiAgIHZlcnNpb249IjEuMSIKICAgdmlld0JveD0iMCAwIDI2LjQ1ODMzMjcyNDUgMjYuNDU4MzM0MDgxIgogICBoZWlnaHQ9IjEwMCIKICAgd2lkdGg9IjEwMCI+CiAgPGRlZnMKICAgICBpZD0iZGVmczIiIC8+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhNSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgICA8ZGM6dGl0bGU+PC9kYzp0aXRsZT4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGcKICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0yNzAuNTQxNjQ5ODgyKSIKICAgICBpZD0ibGF5ZXIxIj4KICAgIDxjaXJjbGUKICAgICAgIHI9IjEyLjcyOTE2Njk4NDYiCiAgICAgICBjeT0iMjgzLjc3MDgxMjk4OCIKICAgICAgIGN4PSIxMy4yMjkxNjY5ODQ2IgogICAgICAgaWQ9InBhdGg0NTE4IgogICAgICAgc3R5bGU9Im9wYWNpdHk6MTtmaWxsOm5vbmU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjE7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MSIgLz4KICAgIDxwYXRoCiAgICAgICBkPSJNIDEwLjQ1MTA0MTY2NjYsMjgwLjcxNzkzMTk0NyBIIDE2LjI3MTg3NSBtIC04LjE0OTE2NjY2NjgsNS42OTg3MTc5MzUgOC43MzEyNSwtMy43MDQxNjY2NTcgSCA5Ljg2ODk1ODMzMzE3IGwgOC40NDAyMDgzMzM0MywzLjcwNDE2NjY1NyBtIC0xMC4xODY0NTgzMzM0LDAgSCAxOC4zMDkxNjY2NjY2IE0gMTYuMjcxODc1LDI4MC43MTc5MzE5NDcgMjAuNjM3NSwyNzYuNDQzODkzNDk2IEggNi4wODU0MTY2NjY1NyBsIDQuMzY1NjI1MDAwMTMsNC4yNzQwMzg0NTEgbSAyLjkxMDQxNjY2NjUsLTcuMTIzMzk4MDQgLTcuMjc2MDQxNjY2NjMsMTguNTIwODMzOTEgTCAxMS45MDYyNSwyODYuNDE2NjQ5ODgyIGggMi45MTA0MTY2NjY2IEwgMjAuNjM3NSwyOTIuMTE1MzY3ODE3IFoiCiAgICAgICBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMDAwMDAwO3N0cm9rZS13aWR0aDowLjY5OTk5OTk4ODE7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmU7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgIGlkPSJwYXRoNDU2NCIgLz4KICA8L2c+Cjwvc3ZnPgo=" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="4" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" direction="0" opacity="1" lineSizeScale="3x:0,0,0,0,0,0" minimumSize="0" penWidth="0" height="15" penAlpha="255" backgroundAlpha="255" enabled="0" scaleBasedVisibility="0" minScaleDenominator="0" barWidth="5" lineSizeType="MM" rotationOffset="270" spacing="5" sizeType="MM" scaleDependency="Area" spacingUnit="MM" width="15" maxScaleDenominator="1e+08" penColor="#000000" diagramOrientation="Up" spacingUnitScale="3x:0,0,0,0,0,0" backgroundColor="#ffffff" showAxis="1">
      <fontProperties description="Roboto,11,-1,5,25,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
      <axisSymbol>
        <symbol name="" clip_to_extent="1" type="line" alpha="1" force_rhr="0">
          <layer locked="0" class="SimpleLine" pass="0" enabled="1">
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" name="name" type="QString"/>
                <Option name="properties"/>
                <Option value="collection" name="type" type="QString"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" dist="0" linePlacementFlags="18" priority="0" showAll="1" placement="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="Hidden">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="identifiant" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="code" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionxy" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="precisionz" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="A" name="classe A" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="B" name="classe B" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="C" name="classe C" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="xyschematique" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="visiblesurface" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="sensible" configurationFlags="None">
      <editWidget type="CheckBox">
        <config>
          <Option type="Map">
            <Option value="" name="CheckedState" type="QString"/>
            <Option value="0" name="TextDisplayMethod" type="int"/>
            <Option value="" name="UncheckedState" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="statut" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="functional" name="Actif" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="projected" name="En projet" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underConstruction" name="En cours de construction/modification" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="decommissioned" name="Déclassé" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validede" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="validejusque" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="positionverticale" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="onGroundSurface" name="au niveau du sol" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="suspendedOrElevated" name="suspendu ou surelevé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="underground" name="sous le sol" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="miseajour" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option type="Map">
            <Option value="true" name="allow_null" type="bool"/>
            <Option value="true" name="calendar_popup" type="bool"/>
            <Option value="yyyy-MM-dd" name="display_format" type="QString"/>
            <Option value="yyyy-MM-dd" name="field_format" type="QString"/>
            <Option value="false" name="field_iso_format" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="dimension" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="caracteristiques" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="idaffleurant" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="materiau" configurationFlags="None">
      <editWidget type="ValueMap">
        <config>
          <Option type="Map">
            <Option name="map" type="List">
              <Option type="Map">
                <Option value="ABS" name="ABS" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="asbestos" name="amiante" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="blackIron" name="fer noir" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="blackSteel" name="acier noir" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="castIron" name="fonte" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="clay" name="argile" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="compositeConcrete" name="béton composite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="concrete" name="béton" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="CPVC" name="PVCC" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="FRP" name="plastique renforcé de fibres (FRP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="galvanizedSteel" name="métal galvanisé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="masonry" name="maçonnerie" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="other" name="autre" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PB" name="polybutylène (PB)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PE" name="polyéthylène (PE)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PEX" name="polyéthylène réticulé à haute densité (PEX)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PP" name="polypropylène (PP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="prestressedReinforcedConcrete" name="béton armé précontraint" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="PVC" name="PVC" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="reinforcedConcrete" name="béton renforcé" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="RPMP" name="mortier renforcé de polymères(RPMP)" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="steel" name="acier" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="terracota" name="terre cuite" type="QString"/>
              </Option>
              <Option type="Map">
                <Option value="wood" name="bois" type="QString"/>
              </Option>
            </Option>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="hauteurpylone" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option value="false" name="IsMultiline" type="bool"/>
            <Option value="false" name="UseHtml" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="idouvrage" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option type="Map">
            <Option value="true" name="AllowNull" type="bool"/>
            <Option value="2147483647" name="Max" type="int"/>
            <Option value="-2147483648" name="Min" type="int"/>
            <Option value="0" name="Precision" type="int"/>
            <Option value="1" name="Step" type="int"/>
            <Option value="SpinBox" name="Style" type="QString"/>
          </Option>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" field="fid" index="0"/>
    <alias name="" field="identifiant" index="1"/>
    <alias name="" field="code" index="2"/>
    <alias name="" field="precisionxy" index="3"/>
    <alias name="" field="precisionz" index="4"/>
    <alias name="" field="xyschematique" index="5"/>
    <alias name="" field="visiblesurface" index="6"/>
    <alias name="" field="sensible" index="7"/>
    <alias name="" field="statut" index="8"/>
    <alias name="" field="validede" index="9"/>
    <alias name="" field="validejusque" index="10"/>
    <alias name="" field="positionverticale" index="11"/>
    <alias name="" field="miseajour" index="12"/>
    <alias name="" field="dimension" index="13"/>
    <alias name="" field="caracteristiques" index="14"/>
    <alias name="" field="idaffleurant" index="15"/>
    <alias name="" field="materiau" index="16"/>
    <alias name="hauteur (en m)" field="hauteurpylone" index="17"/>
    <alias name="" field="idouvrage" index="18"/>
  </aliases>
  <defaults>
    <default field="fid" expression="" applyOnUpdate="0"/>
    <default field="identifiant" expression="" applyOnUpdate="0"/>
    <default field="code" expression="" applyOnUpdate="0"/>
    <default field="precisionxy" expression="" applyOnUpdate="0"/>
    <default field="precisionz" expression="" applyOnUpdate="0"/>
    <default field="xyschematique" expression="" applyOnUpdate="0"/>
    <default field="visiblesurface" expression="" applyOnUpdate="0"/>
    <default field="sensible" expression="" applyOnUpdate="0"/>
    <default field="statut" expression="" applyOnUpdate="0"/>
    <default field="validede" expression="" applyOnUpdate="0"/>
    <default field="validejusque" expression="" applyOnUpdate="0"/>
    <default field="positionverticale" expression="" applyOnUpdate="0"/>
    <default field="miseajour" expression="" applyOnUpdate="0"/>
    <default field="dimension" expression="" applyOnUpdate="0"/>
    <default field="caracteristiques" expression="" applyOnUpdate="0"/>
    <default field="idaffleurant" expression="" applyOnUpdate="0"/>
    <default field="materiau" expression="" applyOnUpdate="0"/>
    <default field="hauteurpylone" expression="" applyOnUpdate="0"/>
    <default field="idouvrage" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" field="fid" constraints="3" unique_strength="1" notnull_strength="1"/>
    <constraint exp_strength="0" field="identifiant" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="code" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionxy" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="precisionz" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="xyschematique" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="visiblesurface" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="sensible" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="statut" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validede" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="validejusque" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="positionverticale" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="miseajour" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="dimension" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="caracteristiques" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="idaffleurant" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="materiau" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="hauteurpylone" constraints="0" unique_strength="0" notnull_strength="0"/>
    <constraint exp_strength="0" field="idouvrage" constraints="0" unique_strength="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="fid" desc="" exp=""/>
    <constraint field="identifiant" desc="" exp=""/>
    <constraint field="code" desc="" exp=""/>
    <constraint field="precisionxy" desc="" exp=""/>
    <constraint field="precisionz" desc="" exp=""/>
    <constraint field="xyschematique" desc="" exp=""/>
    <constraint field="visiblesurface" desc="" exp=""/>
    <constraint field="sensible" desc="" exp=""/>
    <constraint field="statut" desc="" exp=""/>
    <constraint field="validede" desc="" exp=""/>
    <constraint field="validejusque" desc="" exp=""/>
    <constraint field="positionverticale" desc="" exp=""/>
    <constraint field="miseajour" desc="" exp=""/>
    <constraint field="dimension" desc="" exp=""/>
    <constraint field="caracteristiques" desc="" exp=""/>
    <constraint field="idaffleurant" desc="" exp=""/>
    <constraint field="materiau" desc="" exp=""/>
    <constraint field="hauteurpylone" desc="" exp=""/>
    <constraint field="idouvrage" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column name="fid" hidden="0" type="field" width="-1"/>
      <column name="identifiant" hidden="0" type="field" width="-1"/>
      <column name="code" hidden="0" type="field" width="-1"/>
      <column name="precisionxy" hidden="0" type="field" width="-1"/>
      <column name="precisionz" hidden="0" type="field" width="-1"/>
      <column name="xyschematique" hidden="0" type="field" width="-1"/>
      <column name="visiblesurface" hidden="0" type="field" width="-1"/>
      <column name="sensible" hidden="0" type="field" width="-1"/>
      <column name="statut" hidden="0" type="field" width="-1"/>
      <column name="validede" hidden="0" type="field" width="-1"/>
      <column name="validejusque" hidden="0" type="field" width="-1"/>
      <column name="positionverticale" hidden="0" type="field" width="-1"/>
      <column name="miseajour" hidden="0" type="field" width="-1"/>
      <column name="dimension" hidden="0" type="field" width="-1"/>
      <column name="caracteristiques" hidden="0" type="field" width="-1"/>
      <column name="idaffleurant" hidden="0" type="field" width="-1"/>
      <column name="materiau" hidden="0" type="field" width="-1"/>
      <column name="hauteurpylone" hidden="0" type="field" width="-1"/>
      <column name="idouvrage" hidden="0" type="field" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>tablayout</editorlayout>
  <attributeEditorForm>
    <attributeEditorField name="idaffleurant" showLabel="1" index="15"/>
    <attributeEditorField name="hauteurpylone" showLabel="1" index="17"/>
    <attributeEditorContainer name="Element Reseau" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="identifiant" showLabel="1" index="1"/>
      <attributeEditorField name="code" showLabel="1" index="2"/>
      <attributeEditorField name="precisionxy" showLabel="1" index="3"/>
      <attributeEditorField name="precisionz" showLabel="1" index="4"/>
      <attributeEditorField name="xyschematique" showLabel="1" index="5"/>
      <attributeEditorField name="visiblesurface" showLabel="1" index="6"/>
      <attributeEditorField name="sensible" showLabel="1" index="7"/>
    </attributeEditorContainer>
    <attributeEditorContainer name="Ouvrage" visibilityExpressionEnabled="0" showLabel="1" groupBox="0" visibilityExpression="" columnCount="1">
      <attributeEditorField name="statut" showLabel="1" index="8"/>
      <attributeEditorField name="materiau" showLabel="1" index="16"/>
      <attributeEditorField name="dimension" showLabel="1" index="13"/>
      <attributeEditorField name="caracteristiques" showLabel="1" index="14"/>
      <attributeEditorField name="positionverticale" showLabel="1" index="11"/>
      <attributeEditorField name="validede" showLabel="1" index="9"/>
      <attributeEditorField name="validejusque" showLabel="1" index="10"/>
    </attributeEditorContainer>
    <attributeEditorField name="miseajour" showLabel="1" index="12"/>
  </attributeEditorForm>
  <editable>
    <field name="caracteristiques" editable="1"/>
    <field name="code" editable="1"/>
    <field name="dimension" editable="1"/>
    <field name="fid" editable="0"/>
    <field name="hauteurpylone" editable="1"/>
    <field name="idaffleurant" editable="1"/>
    <field name="identifiant" editable="1"/>
    <field name="idouvrage" editable="1"/>
    <field name="materiau" editable="1"/>
    <field name="miseajour" editable="0"/>
    <field name="positionverticale" editable="1"/>
    <field name="precisionxy" editable="1"/>
    <field name="precisionz" editable="1"/>
    <field name="sensible" editable="1"/>
    <field name="statut" editable="1"/>
    <field name="validede" editable="1"/>
    <field name="validejusque" editable="1"/>
    <field name="visiblesurface" editable="1"/>
    <field name="xyschematique" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="caracteristiques" labelOnTop="0"/>
    <field name="code" labelOnTop="0"/>
    <field name="dimension" labelOnTop="0"/>
    <field name="fid" labelOnTop="0"/>
    <field name="hauteurpylone" labelOnTop="0"/>
    <field name="idaffleurant" labelOnTop="0"/>
    <field name="identifiant" labelOnTop="0"/>
    <field name="idouvrage" labelOnTop="0"/>
    <field name="materiau" labelOnTop="0"/>
    <field name="miseajour" labelOnTop="0"/>
    <field name="positionverticale" labelOnTop="0"/>
    <field name="precisionxy" labelOnTop="0"/>
    <field name="precisionz" labelOnTop="0"/>
    <field name="sensible" labelOnTop="0"/>
    <field name="statut" labelOnTop="0"/>
    <field name="validede" labelOnTop="0"/>
    <field name="validejusque" labelOnTop="0"/>
    <field name="visiblesurface" labelOnTop="0"/>
    <field name="xyschematique" labelOnTop="0"/>
  </labelOnTop>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"fid"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
