--
-- PostgreSQL database dump
--
-- Dumped from database version 11.14 (Debian 11.14-0+deb10u1)
-- Dumped by pg_dump version 11.5 (Debian 11.5-1+deb10u1)
-- Started on 2022-04-23 23:12:33 CEST
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
--
-- TOC entry 7 (class 2615 OID 3384893)
-- Name: StaRDT; Type: SCHEMA; Schema: -; Owner: -
--
CREATE SCHEMA "StaRDT";
SET default_tablespace = '';
SET default_with_oids = false;
--
-- TOC entry 223 (class 1259 OID 3384896)
-- Name: elementreseau; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".elementreseau (
    identifiant integer NOT NULL,
    code character varying,
    precisionxy character varying,
    precisionz character varying,
    xyschematique boolean,
    visiblesurface boolean,
    sensible boolean
);
--
-- TOC entry 4919 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE elementreseau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".elementreseau IS 'classe abstraite regroupe l''ensemble des propriétés du réseau.';
--
-- TOC entry 4920 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN elementreseau.identifiant; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".elementreseau.identifiant IS 'Identifiant de l''objet';
--
-- TOC entry 4921 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN elementreseau.code; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".elementreseau.code IS 'Code métier associé à l''objet dans la base de référence de l''exploitant.';
--
-- TOC entry 4922 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN elementreseau.precisionxy; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".elementreseau.precisionxy IS 'Indication de la précision dans le plan horizontal (x,y) de la position du géométrique de l''élément.';
--
-- TOC entry 4923 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN elementreseau.precisionz; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".elementreseau.precisionz IS 'Indication de la précision dans le plan vertical (z) de la position du géométrique de l''élément.';
--
-- TOC entry 4924 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN elementreseau.xyschematique; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".elementreseau.xyschematique IS 'Précise si les coordonnées sont des coordonnées graphiques (par
opposition à des coordonnées vraies)';
--
-- TOC entry 4925 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN elementreseau.visiblesurface; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".elementreseau.visiblesurface IS 'Indique si l''élément est visible au-dessus du niveau du sol';
--
-- TOC entry 4926 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN elementreseau.sensible; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".elementreseau.sensible IS 'Indique s''il s''agit d''un ouvrage sensible selon la définition de la réglementation DT-DICT.';
--
-- TOC entry 224 (class 1259 OID 3384905)
-- Name: ouvrage; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".ouvrage (
    statut character varying,
    validede date,
    validejusque date,
    positionverticale character varying,
    miseajour date DEFAULT now(),
    dimension numeric,
    caracteristiques character varying,
    idouvrage integer NOT NULL
)
INHERITS ("StaRDT".elementreseau);
--
-- TOC entry 4927 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE ouvrage; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".ouvrage IS 'Classe abstraite, tout ou partie de canalisation, ligne, installation
appartenant à une des catégories mentionnées au I ou au II de l’article R.
554-2 ainsi que leurs branchements et équipements ou accessoires
nécessaires à leur fonctionnement;';
--
-- TOC entry 4928 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN ouvrage.statut; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrage.statut IS 'Statut de l''objet concernant son état et son usage';
--
-- TOC entry 4929 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN ouvrage.validede; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrage.validede IS 'Date de création de l''ouvrage dans le monde réel';
--
-- TOC entry 4930 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN ouvrage.validejusque; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrage.validejusque IS 'Date de destruction de l''objet dans le monde réél.';
--
-- TOC entry 4931 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN ouvrage.positionverticale; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrage.positionverticale IS 'Position de l''ouvrage par rapport au sol';
--
-- TOC entry 4932 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN ouvrage.miseajour; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrage.miseajour IS 'Date de dernière modification de l''objet dans la base de référence';
--
-- TOC entry 4933 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN ouvrage.dimension; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrage.dimension IS 'Dimension de l''ouvrage';
--
-- TOC entry 4934 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN ouvrage.caracteristiques; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrage.caracteristiques IS 'Caractéristiques techniques de l''ouvrage';
--
-- TOC entry 246 (class 1259 OID 3385125)
-- Name: noeudreseau; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".noeudreseau (
    geometrie public.geometry(Point,2154)
)
INHERITS ("StaRDT".ouvrage);
--
-- TOC entry 4935 (class 0 OID 0)
-- Dependencies: 246
-- Name: TABLE noeudreseau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".noeudreseau IS 'Point de rupture entre deux tronçons d''ouvrage consécutifs.';
--
-- TOC entry 247 (class 1259 OID 3385132)
-- Name: accessoire; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".accessoire (
    typeaccessoire character varying
)
INHERITS ("StaRDT".noeudreseau);
--
-- TOC entry 4936 (class 0 OID 0)
-- Dependencies: 247
-- Name: TABLE accessoire; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".accessoire IS 'Classe qui définit les accessoires du réseau.';
--
-- TOC entry 4937 (class 0 OID 0)
-- Dependencies: 247
-- Name: COLUMN accessoire.typeaccessoire; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".accessoire.typeaccessoire IS 'Le type d’équipement décrit';
--
-- TOC entry 237 (class 1259 OID 3385064)
-- Name: affleurant; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".affleurant (
    idaffleurant character varying NOT NULL
);
--
-- TOC entry 4938 (class 0 OID 0)
-- Dependencies: 237
-- Name: TABLE affleurant; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".affleurant IS 'Affleurants du réseau';
--
-- TOC entry 4939 (class 0 OID 0)
-- Dependencies: 237
-- Name: COLUMN affleurant.idaffleurant; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".affleurant.idaffleurant IS 'Identifiant qui fait le lien avec l''identifiant renseigné dans le PCRS.';
--
-- TOC entry 225 (class 1259 OID 3384912)
-- Name: troncon; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".troncon (
    typeelement character varying,
    dispositifprotection character varying,
    profondeurminreg numeric,
    profondeurminnonreg numeric,
    materiau character varying,
    hierarchie character varying,
    commentaire character varying,
    hauteurminreg numeric,
    exemptionic boolean,
    geometrie public.geometry(LineString,2154)
)
INHERITS ("StaRDT".ouvrage);
--
-- TOC entry 4940 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE troncon; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".troncon IS 'Classe abstraite qui regroupe les propriétés des linéaires de réseau câble,
fourreau et conduite.';
--
-- TOC entry 4941 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.typeelement; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.typeelement IS 'Type d''élément de réseau (transport, distribution, collecte ...)';
--
-- TOC entry 4942 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.dispositifprotection; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.dispositifprotection IS 'Dispositif permettant de protéger le tronçon d''ouvrage contre les
agressions externes';
--
-- TOC entry 4943 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.profondeurminreg; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.profondeurminreg IS 'Profondeur réglementaire minimale à la génératrice supérieure';
--
-- TOC entry 4944 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.profondeurminnonreg; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.profondeurminnonreg IS 'Profondeur minimale à la génératrice supérieure.';
--
-- TOC entry 4945 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.materiau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.materiau IS 'Matériau du tronçon';
--
-- TOC entry 4946 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.hierarchie; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.hierarchie IS 'Hiérarchie du tronçon dans le réseau.';
--
-- TOC entry 4947 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.commentaire; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.commentaire IS 'Tout type de commentaire additionnel utile.';
--
-- TOC entry 4948 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.hauteurminreg; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.hauteurminreg IS 'Hauteur minimale réglementaire';
--
-- TOC entry 4949 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN troncon.exemptionic; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".troncon.exemptionic IS 'Tronçon pour lequel l’obligation de réponse en classe A à la Déclaration de
Travaux ne s’applique pas';
--
-- TOC entry 226 (class 1259 OID 3384922)
-- Name: cable; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".cable (
)
INHERITS ("StaRDT".troncon);
--
-- TOC entry 4950 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE cable; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".cable IS 'Classe abstraite qui regroupe les tronçons ou séquence de tronçons qui
permettent de connecter électriquement un endroit à un autre.';
--
-- TOC entry 230 (class 1259 OID 3384947)
-- Name: autrecable; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".autrecable (
)
INHERITS ("StaRDT".cable);
--
-- TOC entry 4951 (class 0 OID 0)
-- Dependencies: 230
-- Name: TABLE autrecable; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".autrecable IS 'Classe abstraite qui regroupe les tronçons ou séquence de tronçons qui
permettent de connecter électriquement un endroit à un autre.';
--
-- TOC entry 231 (class 1259 OID 3384954)
-- Name: canalisation; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".canalisation (
)
INHERITS ("StaRDT".troncon);
--
-- TOC entry 4952 (class 0 OID 0)
-- Dependencies: 231
-- Name: TABLE canalisation; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".canalisation IS 'Classe abstraite qui regroupe les tronçons de services d''utilité publique pour le transport des solides, liquides, produits chimiques ou gaz d''un endroit à un autre.';
--
-- TOC entry 249 (class 1259 OID 3385147)
-- Name: autrecanalisation; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".autrecanalisation (
    produittransporte character varying,
    pression numeric
)
INHERITS ("StaRDT".canalisation);
--
-- TOC entry 4953 (class 0 OID 0)
-- Dependencies: 249
-- Name: TABLE autrecanalisation; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".autrecanalisation IS 'Canalisation dont le type est indéterminé ou non couvert pas les autres
types de conduite.';
--
-- TOC entry 238 (class 1259 OID 3385070)
-- Name: conteneur; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".conteneur (
    materiau character varying NOT NULL,
    geometrie public.geometry(Point,2154)
)
INHERITS ("StaRDT".ouvrage, "StaRDT".affleurant);
--
-- TOC entry 4954 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE conteneur; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".conteneur IS 'Classe abstraite qui regroupe les conteneurs de noeuds, qui servent de
support aux noeuds du réseaux.';
--
-- TOC entry 4955 (class 0 OID 0)
-- Dependencies: 238
-- Name: COLUMN conteneur.materiau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".conteneur.materiau IS 'materiau';
--
-- TOC entry 244 (class 1259 OID 3385112)
-- Name: batimenttechnique; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".batimenttechnique (
)
INHERITS ("StaRDT".conteneur);
--
-- TOC entry 4956 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE batimenttechnique; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".batimenttechnique IS 'Bâtiment hébergeant des équipements permettant d''assurer diverses
fonctions du réseau : coupure, comptage, transformation de tension... A la
différence de l’armoire/coffret, il est possible d’entrer à l’intérieur.';
--
-- TOC entry 227 (class 1259 OID 3384929)
-- Name: cableelectrique; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".cableelectrique (
    classetension character varying,
    fonctioncable character varying,
    regime character varying
)
INHERITS ("StaRDT".cable);
--
-- TOC entry 4957 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE cableelectrique; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".cableelectrique IS 'Liaison utilisée pour acheminer l''électricité d''un endroit à un autre';
--
-- TOC entry 4958 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN cableelectrique.classetension; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".cableelectrique.classetension IS 'classe de tension issu de la norme NF C 18-510';
--
-- TOC entry 4959 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN cableelectrique.fonctioncable; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".cableelectrique.fonctioncable IS 'fonction du câble électrique';
--
-- TOC entry 4960 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN cableelectrique.regime; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".cableelectrique.regime IS 'régime du câble électrique';
--
-- TOC entry 229 (class 1259 OID 3384940)
-- Name: cabletelecommunication; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".cabletelecommunication (
    technocable character varying
)
INHERITS ("StaRDT".cable);
--
-- TOC entry 4961 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE cabletelecommunication; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".cabletelecommunication IS 'Liaison utilisée pour acheminer des signaux de données d''un endroit à un
autre';
--
-- TOC entry 251 (class 1259 OID 3385161)
-- Name: canalisationassainissementpluviale; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".canalisationassainissementpluviale (
    typeassainissement character varying,
    ecoulement character varying
)
INHERITS ("StaRDT".canalisation);
--
-- TOC entry 4962 (class 0 OID 0)
-- Dependencies: 251
-- Name: TABLE canalisationassainissementpluviale; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".canalisationassainissementpluviale IS 'Canalisation utilisée pour transporter des eaux usées ou des eaux pluviales d''un endroit à un autre.';
--
-- TOC entry 4963 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN canalisationassainissementpluviale.typeassainissement; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationassainissementpluviale.typeassainissement IS 'type de conduite d''assainissement';
--
-- TOC entry 4964 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN canalisationassainissementpluviale.ecoulement; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationassainissementpluviale.ecoulement IS 'type d''écoulement';
--
-- TOC entry 250 (class 1259 OID 3385154)
-- Name: canalisationeau; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".canalisationeau (
    typecanalisationeau character varying,
    ecoulement character varying,
    typedepart character varying
)
INHERITS ("StaRDT".canalisation);
--
-- TOC entry 4965 (class 0 OID 0)
-- Dependencies: 250
-- Name: TABLE canalisationeau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".canalisationeau IS 'Conduite utilisée pour transporter de l''eau d''un endroit à un autre.';
--
-- TOC entry 4966 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN canalisationeau.typecanalisationeau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationeau.typecanalisationeau IS 'Type de canalisation d''eau';
--
-- TOC entry 4967 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN canalisationeau.ecoulement; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationeau.ecoulement IS 'Type d''écoulement de l''eau';
--
-- TOC entry 4968 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN canalisationeau.typedepart; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationeau.typedepart IS 'Type de départ (Branchement seulement)';
--
-- TOC entry 252 (class 1259 OID 3385168)
-- Name: canalisationhydrocarburechimique; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".canalisationhydrocarburechimique (
    typefluide character varying,
    classetemperature character varying,
    pression numeric
)
INHERITS ("StaRDT".canalisation);
--
-- TOC entry 4969 (class 0 OID 0)
-- Dependencies: 252
-- Name: TABLE canalisationhydrocarburechimique; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".canalisationhydrocarburechimique IS 'Canalisation utilisée pour transporter des hydrocarbures ou des produits chimiques d''un endroit à un autre.';
--
-- TOC entry 4970 (class 0 OID 0)
-- Dependencies: 252
-- Name: COLUMN canalisationhydrocarburechimique.typefluide; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationhydrocarburechimique.typefluide IS 'Type de fluide transporté';
--
-- TOC entry 4971 (class 0 OID 0)
-- Dependencies: 252
-- Name: COLUMN canalisationhydrocarburechimique.classetemperature; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationhydrocarburechimique.classetemperature IS 'Classe de température du fluide transporté';
--
-- TOC entry 4972 (class 0 OID 0)
-- Dependencies: 252
-- Name: COLUMN canalisationhydrocarburechimique.pression; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationhydrocarburechimique.pression IS 'Pression réglementaire : maximale en service';
--
-- TOC entry 253 (class 1259 OID 3385175)
-- Name: canalisationthermique; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".canalisationthermique (
    typefluide character varying,
    temperature numeric,
    pression numeric
)
INHERITS ("StaRDT".canalisation);
--
-- TOC entry 4973 (class 0 OID 0)
-- Dependencies: 253
-- Name: TABLE canalisationthermique; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".canalisationthermique IS 'Canalisation utilisée pour diffuser la chaleur ou le froid d''un endroit à un
autre.';
--
-- TOC entry 4974 (class 0 OID 0)
-- Dependencies: 253
-- Name: COLUMN canalisationthermique.typefluide; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationthermique.typefluide IS 'Type de fluide transporté';
--
-- TOC entry 4975 (class 0 OID 0)
-- Dependencies: 253
-- Name: COLUMN canalisationthermique.temperature; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationthermique.temperature IS 'Température en degrés celsius.';
--
-- TOC entry 4976 (class 0 OID 0)
-- Dependencies: 253
-- Name: COLUMN canalisationthermique.pression; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".canalisationthermique.pression IS 'Pression réglementaire : maximale en service';
--
-- TOC entry 239 (class 1259 OID 3385077)
-- Name: coffret; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".coffret (
    fonction character varying NOT NULL
)
INHERITS ("StaRDT".conteneur);
--
-- TOC entry 4977 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE coffret; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".coffret IS 'Objet se présentant sous la forme d''une simple armoire qui peut comporter
des objets de services d''utilité publique appartenant à un ou plusieurs
réseaux de services d''utilité publique.';
--
-- TOC entry 4978 (class 0 OID 0)
-- Dependencies: 239
-- Name: COLUMN coffret.fonction; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".coffret.fonction IS 'Fonction de l''armoire/du coffret';
--
-- TOC entry 254 (class 1259 OID 3385188)
-- Name: etiquette; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".etiquette (
    etiquette character varying,
    detail character varying
);
--
-- TOC entry 4979 (class 0 OID 0)
-- Dependencies: 254
-- Name: TABLE etiquette; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".etiquette IS 'classe abstraite permettant d''ajouter des étiquettes';
--
-- TOC entry 4980 (class 0 OID 0)
-- Dependencies: 254
-- Name: COLUMN etiquette.etiquette; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".etiquette.etiquette IS 'Texte ou nombre décrivant ou quantifiant une propriété et affiché sous forme d''annotation sur une image de carte.';
--
-- TOC entry 4981 (class 0 OID 0)
-- Dependencies: 254
-- Name: COLUMN etiquette.detail; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".etiquette.detail IS 'Description détaillée de la nature de l''information.';
--
-- TOC entry 256 (class 1259 OID 3385200)
-- Name: informationsupplementaire; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".informationsupplementaire (
    fk_elementreseau integer
)
INHERITS ("StaRDT".etiquette);
--
-- TOC entry 4982 (class 0 OID 0)
-- Dependencies: 256
-- Name: TABLE informationsupplementaire; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".informationsupplementaire IS 'Classe abstraite qui regroupe les informations complémentaires à
l''échange';
--
-- TOC entry 257 (class 1259 OID 3385206)
-- Name: cote; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".cote (
    typecote character varying,
    anglerotation numeric,
    localisation public.geometry(LineString,2154)
)
INHERITS ("StaRDT".informationsupplementaire);
--
-- TOC entry 4983 (class 0 OID 0)
-- Dependencies: 257
-- Name: TABLE cote; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".cote IS 'Les côtes permettent de positionner un ouvrage relativement à des éléments du fond de plan.';
--
-- TOC entry 4984 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN cote.typecote; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".cote.typecote IS 'Permet de préciser le type d’objet constituant une côte';
--
-- TOC entry 4985 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN cote.anglerotation; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".cote.anglerotation IS 'Angle de roation du symbole ou de l''étiquette';
--
-- TOC entry 4986 (class 0 OID 0)
-- Dependencies: 257
-- Name: COLUMN cote.localisation; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".cote.localisation IS 'point d''ancrage de la côte ou linéaire décrivant la prise de la côte';
--
-- TOC entry 222 (class 1259 OID 3384894)
-- Name: elementreseau_identifiant_seq; Type: SEQUENCE; Schema: StaRDT; Owner: -
--
CREATE SEQUENCE "StaRDT".elementreseau_identifiant_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
--
-- TOC entry 4987 (class 0 OID 0)
-- Dependencies: 222
-- Name: elementreseau_identifiant_seq; Type: SEQUENCE OWNED BY; Schema: StaRDT; Owner: -
--
ALTER SEQUENCE "StaRDT".elementreseau_identifiant_seq OWNED BY "StaRDT".elementreseau.identifiant;
--
-- TOC entry 232 (class 1259 OID 3384962)
-- Name: enveloppecableconduite; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".enveloppecableconduite (
    nombreouvrages integer
)
INHERITS ("StaRDT".troncon);
--
-- TOC entry 4988 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE enveloppecableconduite; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".enveloppecableconduite IS 'Classe abstraite qui regroupe les tronçons constituant une construction
dans laquelle les câbles et les canalisations sont protégés et guidés.';
--
-- TOC entry 4989 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN enveloppecableconduite.nombreouvrages; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".enveloppecableconduite.nombreouvrages IS 'Nombre de câbles, conduites ou canalisations dans l''élément conteneur.';
--
-- TOC entry 233 (class 1259 OID 3384969)
-- Name: fourreau; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".fourreau (
)
INHERITS ("StaRDT".enveloppecableconduite);
--
-- TOC entry 4990 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE fourreau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".fourreau IS 'Fourreau qui contient des câbles et canalisations';
--
-- TOC entry 236 (class 1259 OID 3385057)
-- Name: galerie; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".galerie (
    largeur numeric,
    hauteur numeric
)
INHERITS ("StaRDT".enveloppecableconduite);
--
-- TOC entry 4991 (class 0 OID 0)
-- Dependencies: 236
-- Name: TABLE galerie; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".galerie IS 'Infrastructure servant à protéger et à guider les câbles et les tuyaux au
moyen d''une construction enveloppante.';
--
-- TOC entry 4992 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN galerie.largeur; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".galerie.largeur IS 'Largeur de la galerie';
--
-- TOC entry 4993 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN galerie.hauteur; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".galerie.hauteur IS 'Hauteur de la galerie';
--
-- TOC entry 260 (class 1259 OID 3385225)
-- Name: geometriesupplementaire_ligne2d; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".geometriesupplementaire_ligne2d (
    precisionxy character varying,
    precisionz character varying,
    commentaire character varying,
    geometrie public.geometry(LineStringZ,2154),
    fk_elementreseau integer
);
--
-- TOC entry 4994 (class 0 OID 0)
-- Dependencies: 260
-- Name: TABLE geometriesupplementaire_ligne2d; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".geometriesupplementaire_ligne2d IS 'Géométrie supplémentaire en plus de celle déjà oligatoire, qui toutes deux permettent de mieux décrire l''ouvrage.';
--
-- TOC entry 261 (class 1259 OID 3385231)
-- Name: geometriesupplementaire_point2d; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".geometriesupplementaire_point2d (
    precisionxy character varying,
    precisionz character varying,
    commentaire character varying,
    geometrie public.geometry(PointZ,2154),
    fk_elementreseau integer
);
--
-- TOC entry 4995 (class 0 OID 0)
-- Dependencies: 261
-- Name: TABLE geometriesupplementaire_point2d; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".geometriesupplementaire_point2d IS 'Géométrie supplémentaire en plus de celle déjà oligatoire, qui toutes deux permettent de mieux décrire l''ouvrage.';
--
-- TOC entry 258 (class 1259 OID 3385212)
-- Name: geometriesupplementaire_surface2d; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".geometriesupplementaire_surface2d (
    precisionxy character varying,
    precisionz character varying,
    commentaire character varying,
    geometrie public.geometry(Polygon,2154),
    fk_elementreseau integer
);
--
-- TOC entry 4996 (class 0 OID 0)
-- Dependencies: 258
-- Name: TABLE geometriesupplementaire_surface2d; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".geometriesupplementaire_surface2d IS 'Géométrie supplémentaire en plus de celle déjà oligatoire, qui toutes deux permettent de mieux décrire l''ouvrage.';
--
-- TOC entry 259 (class 1259 OID 3385218)
-- Name: geometriesupplementaire_surface3d; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".geometriesupplementaire_surface3d (
    precisionxy character varying,
    precisionz character varying,
    commentaire character varying,
    geometrie public.geometry(PolygonZ,2154),
    fk_elementreseau integer
);
--
-- TOC entry 4997 (class 0 OID 0)
-- Dependencies: 259
-- Name: TABLE geometriesupplementaire_surface3d; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".geometriesupplementaire_surface3d IS 'Géométrie supplémentaire en plus de celle déjà oligatoire, qui toutes deux permettent de mieux décrire l''ouvrage.';
--
-- TOC entry 235 (class 1259 OID 3384983)
-- Name: nappecable; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".nappecable (
    largeur numeric
)
INHERITS ("StaRDT".enveloppecableconduite);
--
-- TOC entry 4998 (class 0 OID 0)
-- Dependencies: 235
-- Name: TABLE nappecable; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".nappecable IS 'Espace formé par le tracé commun d''un ou de plusieurs câbles, tubes,
PEHD et/ou tubes de gaines appartenant à un même opérateur de réseau.';
--
-- TOC entry 4999 (class 0 OID 0)
-- Dependencies: 235
-- Name: COLUMN nappecable.largeur; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".nappecable.largeur IS 'Largeur de la nappe';
--
-- TOC entry 265 (class 1259 OID 3385358)
-- Name: ouvrage_idouvrage_seq; Type: SEQUENCE; Schema: StaRDT; Owner: -
--
CREATE SEQUENCE "StaRDT".ouvrage_idouvrage_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
--
-- TOC entry 5000 (class 0 OID 0)
-- Dependencies: 265
-- Name: ouvrage_idouvrage_seq; Type: SEQUENCE OWNED BY; Schema: StaRDT; Owner: -
--
ALTER SEQUENCE "StaRDT".ouvrage_idouvrage_seq OWNED BY "StaRDT".ouvrage.idouvrage;
--
-- TOC entry 264 (class 1259 OID 3385273)
-- Name: ouvrageentechniquealternative; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".ouvrageentechniquealternative (
    nature character varying
)
INHERITS ("StaRDT".troncon);
--
-- TOC entry 5001 (class 0 OID 0)
-- Dependencies: 264
-- Name: TABLE ouvrageentechniquealternative; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".ouvrageentechniquealternative IS 'Classe regroupant les éléments du réseau d''eaux pluviales qui ne rentrent
pas dans les autres types de tronçons.';
--
-- TOC entry 5002 (class 0 OID 0)
-- Dependencies: 264
-- Name: COLUMN ouvrageentechniquealternative.nature; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".ouvrageentechniquealternative.nature IS 'Nature de l''ouvrage en technique alternative';
--
-- TOC entry 255 (class 1259 OID 3385194)
-- Name: perimetreparticulier; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".perimetreparticulier (
    precaution character varying,
    idinternegestionnaire character varying,
    geometrie public.geometry(Polygon,2154)
)
INHERITS ("StaRDT".etiquette);
--
-- TOC entry 5003 (class 0 OID 0)
-- Dependencies: 255
-- Name: TABLE perimetreparticulier; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".perimetreparticulier IS 'Classe qui définit des périmètres présentant une particularité.';
--
-- TOC entry 5004 (class 0 OID 0)
-- Dependencies: 255
-- Name: COLUMN perimetreparticulier.precaution; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".perimetreparticulier.precaution IS 'Typologie de la particularité';
--
-- TOC entry 5005 (class 0 OID 0)
-- Dependencies: 255
-- Name: COLUMN perimetreparticulier.idinternegestionnaire; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".perimetreparticulier.idinternegestionnaire IS 'Identifiant interne au gestionnaire de la zone. Il peut par exemple s''agir du
code affaire des travaux pour une zone en projet';
--
-- TOC entry 262 (class 1259 OID 3385237)
-- Name: pointlevepcrs; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".pointlevepcrs (
    horodatage date,
    numeropoint character varying,
    precisionxy integer,
    precisionz integer,
    producteur character varying,
    geometrie public.geometry(Point,2154)
);
--
-- TOC entry 263 (class 1259 OID 3385243)
-- Name: pointleveouvragereseau; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".pointleveouvragereseau (
    typeleve character varying,
    leve numeric,
    fk_elementreseau integer
)
INHERITS ("StaRDT".pointlevepcrs);
--
-- TOC entry 5006 (class 0 OID 0)
-- Dependencies: 263
-- Name: TABLE pointleveouvragereseau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".pointleveouvragereseau IS 'décrit les points levés spécifiques au réseau et permet d’indiquer la profondeur ou l’altimétrie connue en certains points des
ouvrages.';
--
-- TOC entry 5007 (class 0 OID 0)
-- Dependencies: 263
-- Name: COLUMN pointleveouvragereseau.typeleve; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".pointleveouvragereseau.typeleve IS 'Précise quel type de levé a été effectué.';
--
-- TOC entry 5008 (class 0 OID 0)
-- Dependencies: 263
-- Name: COLUMN pointleveouvragereseau.leve; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".pointleveouvragereseau.leve IS 'Mesure faite lors du levé';
--
-- TOC entry 240 (class 1259 OID 3385084)
-- Name: poteau; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".poteau (
    hauteurpoteau numeric
)
INHERITS ("StaRDT".conteneur);
--
-- TOC entry 5009 (class 0 OID 0)
-- Dependencies: 240
-- Name: TABLE poteau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".poteau IS 'Objet se présentant sous la forme d''un simple poteau (mât) qui peut supporter des
objets de services d''utilité publique appartenant à un ou plusieurs réseaux de services d''utilité publique.';
--
-- TOC entry 5010 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN poteau.hauteurpoteau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".poteau.hauteurpoteau IS 'Hauteur du poteau';
--
-- TOC entry 248 (class 1259 OID 3385140)
-- Name: protectioninondationsubmersion; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".protectioninondationsubmersion (
    materiau character varying,
    presenceparafouille boolean,
    presencedrainage boolean,
    structureouvrage character varying,
    protectionpiedtalus character varying,
    protectionsurfacetalus character varying,
    typeouvrage character varying,
    caracteristiquescretes text,
    caracteristiquestalusaval text,
    geometrie public.geometry(GeometryCollection,2154)
)
INHERITS ("StaRDT".ouvrage);
--
-- TOC entry 5011 (class 0 OID 0)
-- Dependencies: 248
-- Name: TABLE protectioninondationsubmersion; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".protectioninondationsubmersion IS 'Ouvrages de protection inondation submersion, qui comprennent notamment les digues';
--
-- TOC entry 5012 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.materiau; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.materiau IS 'Matériau principal constituant la digue.';
--
-- TOC entry 5013 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.presenceparafouille; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.presenceparafouille IS 'Indique la présence ou non de protection de type parafouille';
--
-- TOC entry 5014 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.presencedrainage; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.presencedrainage IS 'Indique la présence ou non dun dispositidf de drainage';
--
-- TOC entry 5015 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.structureouvrage; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.structureouvrage IS 'Précise le type de structure de l''ouvrage singulier';
--
-- TOC entry 5016 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.protectionpiedtalus; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.protectionpiedtalus IS 'Précise la nature de la protection au pied du talus.';
--
-- TOC entry 5017 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.protectionsurfacetalus; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.protectionsurfacetalus IS 'Précise la nature de la protection à la surface du talus.';
--
-- TOC entry 5018 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.typeouvrage; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.typeouvrage IS 'Précise le type de l''ouvrage singulier constituant la digue';
--
-- TOC entry 5019 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.caracteristiquescretes; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.caracteristiquescretes IS 'Précise les caractéristiques de la crête du talus';
--
-- TOC entry 5020 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN protectioninondationsubmersion.caracteristiquestalusaval; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".protectioninondationsubmersion.caracteristiquestalusaval IS 'Précise les caractéristiques du talus aval.';
--
-- TOC entry 234 (class 1259 OID 3384976)
-- Name: protectionmecanique; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".protectionmecanique (
)
INHERITS ("StaRDT".enveloppecableconduite);
--
-- TOC entry 5021 (class 0 OID 0)
-- Dependencies: 234
-- Name: TABLE protectionmecanique; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".protectionmecanique IS 'Dispositif de protection mécanique';
--
-- TOC entry 241 (class 1259 OID 3385091)
-- Name: pylone; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".pylone (
    hauteurpylone numeric
)
INHERITS ("StaRDT".conteneur);
--
-- TOC entry 5022 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE pylone; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".pylone IS 'Objet se présentant sous la forme d''un simple pylône qui peut supporter des objets de services d''utilité publique appartenant à un ou plusieurs réseaux de services d''utilité publique.';
--
-- TOC entry 5023 (class 0 OID 0)
-- Dependencies: 241
-- Name: COLUMN pylone.hauteurpylone; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".pylone.hauteurpylone IS 'hauteur du pylone';
--
-- TOC entry 242 (class 1259 OID 3385098)
-- Name: regardnonvisitable; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".regardnonvisitable (
    fonction character varying,
    borgne boolean
)
INHERITS ("StaRDT".conteneur);
--
-- TOC entry 5024 (class 0 OID 0)
-- Dependencies: 242
-- Name: TABLE regardnonvisitable; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".regardnonvisitable IS 'Le regard non visitable est de taille trop limitée pour permettre le passage d’un
humain à l’intérieur.';
--
-- TOC entry 5025 (class 0 OID 0)
-- Dependencies: 242
-- Name: COLUMN regardnonvisitable.fonction; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".regardnonvisitable.fonction IS 'Fonction du regard';
--
-- TOC entry 5026 (class 0 OID 0)
-- Dependencies: 242
-- Name: COLUMN regardnonvisitable.borgne; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".regardnonvisitable.borgne IS 'Prend la valeur « Vrai » si le regard n’a pas d’accès depuis la surface';
--
-- TOC entry 243 (class 1259 OID 3385105)
-- Name: regardvisitable; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".regardvisitable (
    fonction character varying,
    borgne boolean
)
INHERITS ("StaRDT".conteneur);
--
-- TOC entry 5027 (class 0 OID 0)
-- Dependencies: 243
-- Name: TABLE regardvisitable; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".regardvisitable IS 'Espace enterré, suffisamment grand et accessible pour qu’une personne puisse y descendre et intervenir ; exemples : chambre à sable, regard de ventouse...';
--
-- TOC entry 5028 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN regardvisitable.fonction; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".regardvisitable.fonction IS 'Fonction du regard';
--
-- TOC entry 5029 (class 0 OID 0)
-- Dependencies: 243
-- Name: COLUMN regardvisitable.borgne; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".regardvisitable.borgne IS 'Prend la valeur « Vrai » si le regard n’a pas d’accès depuis la surface';
--
-- TOC entry 245 (class 1259 OID 3385119)
-- Name: reseauutilite; Type: TABLE; Schema: StaRDT; Owner: -
--
CREATE TABLE "StaRDT".reseauutilite (
    nom character varying,
    theme character varying,
    profondeurstandard numeric,
    mention character varying,
    responsable character varying,
    fk_elementreseau integer
);
--
-- TOC entry 5030 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE reseauutilite; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON TABLE "StaRDT".reseauutilite IS 'Classe qui permet de décrire le réseau en général.';
--
-- TOC entry 5031 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN reseauutilite.nom; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".reseauutilite.nom IS 'Nom donné au réseau';
--
-- TOC entry 5032 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN reseauutilite.theme; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".reseauutilite.theme IS 'Permet de décrire le type de réseau conformément à la liste des réseaux de la NF P98-332';
--
-- TOC entry 5033 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN reseauutilite.profondeurstandard; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".reseauutilite.profondeurstandard IS 'Profondeur commune associée à ce réseau. La profondeur est relative au
niveau du sol. Elle s''entend à la génératrice supérieure.';
--
-- TOC entry 5034 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN reseauutilite.mention; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".reseauutilite.mention IS 'Mention légale particulière.';
--
-- TOC entry 5035 (class 0 OID 0)
-- Dependencies: 245
-- Name: COLUMN reseauutilite.responsable; Type: COMMENT; Schema: StaRDT; Owner: -
--
COMMENT ON COLUMN "StaRDT".reseauutilite.responsable IS 'Gestionnaire/exploitant du réseau';
--
-- TOC entry 4707 (class 2604 OID 3385135)
-- Name: accessoire identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".accessoire ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4708 (class 2604 OID 3385345)
-- Name: accessoire miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".accessoire ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4709 (class 2604 OID 3385386)
-- Name: accessoire idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".accessoire ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4662 (class 2604 OID 3384950)
-- Name: autrecable identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".autrecable ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4663 (class 2604 OID 3385348)
-- Name: autrecable miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".autrecable ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4664 (class 2604 OID 3385365)
-- Name: autrecable idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".autrecable ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4713 (class 2604 OID 3385150)
-- Name: autrecanalisation identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".autrecanalisation ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4714 (class 2604 OID 3385349)
-- Name: autrecanalisation miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".autrecanalisation ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4715 (class 2604 OID 3385367)
-- Name: autrecanalisation idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".autrecanalisation ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4701 (class 2604 OID 3385115)
-- Name: batimenttechnique identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".batimenttechnique ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4702 (class 2604 OID 3385344)
-- Name: batimenttechnique miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".batimenttechnique ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4703 (class 2604 OID 3385384)
-- Name: batimenttechnique idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".batimenttechnique ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4653 (class 2604 OID 3384925)
-- Name: cable identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cable ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4654 (class 2604 OID 3385335)
-- Name: cable miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cable ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4655 (class 2604 OID 3385362)
-- Name: cable idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cable ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4656 (class 2604 OID 3384932)
-- Name: cableelectrique identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cableelectrique ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4657 (class 2604 OID 3385346)
-- Name: cableelectrique miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cableelectrique ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4658 (class 2604 OID 3385363)
-- Name: cableelectrique idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cableelectrique ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4659 (class 2604 OID 3384943)
-- Name: cabletelecommunication identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cabletelecommunication ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4660 (class 2604 OID 3385347)
-- Name: cabletelecommunication miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cabletelecommunication ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4661 (class 2604 OID 3385364)
-- Name: cabletelecommunication idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".cabletelecommunication ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4665 (class 2604 OID 3384957)
-- Name: canalisation identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisation ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4666 (class 2604 OID 3385336)
-- Name: canalisation miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisation ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4667 (class 2604 OID 3385366)
-- Name: canalisation idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisation ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4719 (class 2604 OID 3385164)
-- Name: canalisationassainissementpluviale identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationassainissementpluviale ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4720 (class 2604 OID 3385351)
-- Name: canalisationassainissementpluviale miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationassainissementpluviale ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4721 (class 2604 OID 3385369)
-- Name: canalisationassainissementpluviale idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationassainissementpluviale ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4716 (class 2604 OID 3385157)
-- Name: canalisationeau identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationeau ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4717 (class 2604 OID 3385350)
-- Name: canalisationeau miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationeau ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4718 (class 2604 OID 3385368)
-- Name: canalisationeau idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationeau ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4722 (class 2604 OID 3385171)
-- Name: canalisationhydrocarburechimique identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationhydrocarburechimique ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4723 (class 2604 OID 3385352)
-- Name: canalisationhydrocarburechimique miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationhydrocarburechimique ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4724 (class 2604 OID 3385370)
-- Name: canalisationhydrocarburechimique idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationhydrocarburechimique ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4726 (class 2604 OID 3385178)
-- Name: canalisationthermique identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationthermique ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4725 (class 2604 OID 3385353)
-- Name: canalisationthermique miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationthermique ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4727 (class 2604 OID 3385371)
-- Name: canalisationthermique idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".canalisationthermique ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4686 (class 2604 OID 3385080)
-- Name: coffret identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".coffret ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4687 (class 2604 OID 3385339)
-- Name: coffret miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".coffret ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4688 (class 2604 OID 3385379)
-- Name: coffret idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".coffret ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4683 (class 2604 OID 3385073)
-- Name: conteneur identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".conteneur ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4684 (class 2604 OID 3385332)
-- Name: conteneur miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".conteneur ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4685 (class 2604 OID 3385378)
-- Name: conteneur idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".conteneur ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4646 (class 2604 OID 3384899)
-- Name: elementreseau identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".elementreseau ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4668 (class 2604 OID 3384965)
-- Name: enveloppecableconduite identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".enveloppecableconduite ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4669 (class 2604 OID 3385337)
-- Name: enveloppecableconduite miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".enveloppecableconduite ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4670 (class 2604 OID 3385372)
-- Name: enveloppecableconduite idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".enveloppecableconduite ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4671 (class 2604 OID 3384972)
-- Name: fourreau identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".fourreau ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4672 (class 2604 OID 3385354)
-- Name: fourreau miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".fourreau ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4673 (class 2604 OID 3385373)
-- Name: fourreau idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".fourreau ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4680 (class 2604 OID 3385060)
-- Name: galerie identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".galerie ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4681 (class 2604 OID 3385357)
-- Name: galerie miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".galerie ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4682 (class 2604 OID 3385376)
-- Name: galerie idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".galerie ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4677 (class 2604 OID 3384986)
-- Name: nappecable identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".nappecable ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4678 (class 2604 OID 3385356)
-- Name: nappecable miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".nappecable ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4679 (class 2604 OID 3385375)
-- Name: nappecable idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".nappecable ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4704 (class 2604 OID 3385128)
-- Name: noeudreseau identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".noeudreseau ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4705 (class 2604 OID 3385333)
-- Name: noeudreseau miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".noeudreseau ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4706 (class 2604 OID 3385385)
-- Name: noeudreseau idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".noeudreseau ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4647 (class 2604 OID 3384908)
-- Name: ouvrage identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".ouvrage ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4649 (class 2604 OID 3385360)
-- Name: ouvrage idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".ouvrage ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4728 (class 2604 OID 3385276)
-- Name: ouvrageentechniquealternative identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".ouvrageentechniquealternative ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4729 (class 2604 OID 3385338)
-- Name: ouvrageentechniquealternative miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".ouvrageentechniquealternative ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4730 (class 2604 OID 3385377)
-- Name: ouvrageentechniquealternative idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".ouvrageentechniquealternative ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4689 (class 2604 OID 3385087)
-- Name: poteau identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".poteau ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4690 (class 2604 OID 3385340)
-- Name: poteau miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".poteau ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4691 (class 2604 OID 3385380)
-- Name: poteau idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".poteau ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4710 (class 2604 OID 3385143)
-- Name: protectioninondationsubmersion identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".protectioninondationsubmersion ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4711 (class 2604 OID 3385334)
-- Name: protectioninondationsubmersion miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".protectioninondationsubmersion ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4712 (class 2604 OID 3385387)
-- Name: protectioninondationsubmersion idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".protectioninondationsubmersion ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4674 (class 2604 OID 3384979)
-- Name: protectionmecanique identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".protectionmecanique ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4675 (class 2604 OID 3385355)
-- Name: protectionmecanique miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".protectionmecanique ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4676 (class 2604 OID 3385374)
-- Name: protectionmecanique idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".protectionmecanique ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4692 (class 2604 OID 3385094)
-- Name: pylone identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".pylone ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4693 (class 2604 OID 3385341)
-- Name: pylone miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".pylone ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4694 (class 2604 OID 3385381)
-- Name: pylone idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".pylone ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4695 (class 2604 OID 3385101)
-- Name: regardnonvisitable identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".regardnonvisitable ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4696 (class 2604 OID 3385342)
-- Name: regardnonvisitable miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".regardnonvisitable ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4697 (class 2604 OID 3385382)
-- Name: regardnonvisitable idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".regardnonvisitable ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4698 (class 2604 OID 3385108)
-- Name: regardvisitable identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".regardvisitable ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4699 (class 2604 OID 3385343)
-- Name: regardvisitable miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".regardvisitable ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4700 (class 2604 OID 3385383)
-- Name: regardvisitable idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".regardvisitable ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4650 (class 2604 OID 3384915)
-- Name: troncon identifiant; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".troncon ALTER COLUMN identifiant SET DEFAULT nextval('"StaRDT".elementreseau_identifiant_seq'::regclass);
--
-- TOC entry 4651 (class 2604 OID 3385331)
-- Name: troncon miseajour; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".troncon ALTER COLUMN miseajour SET DEFAULT now();
--
-- TOC entry 4652 (class 2604 OID 3385361)
-- Name: troncon idouvrage; Type: DEFAULT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".troncon ALTER COLUMN idouvrage SET DEFAULT nextval('"StaRDT".ouvrage_idouvrage_seq'::regclass);
--
-- TOC entry 4894 (class 0 OID 3385132)
-- Dependencies: 247
-- Data for Name: accessoire; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4884 (class 0 OID 3385064)
-- Dependencies: 237
-- Data for Name: affleurant; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4877 (class 0 OID 3384947)
-- Dependencies: 230
-- Data for Name: autrecable; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4896 (class 0 OID 3385147)
-- Dependencies: 249
-- Data for Name: autrecanalisation; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4891 (class 0 OID 3385112)
-- Dependencies: 244
-- Data for Name: batimenttechnique; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4874 (class 0 OID 3384922)
-- Dependencies: 226
-- Data for Name: cable; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4875 (class 0 OID 3384929)
-- Dependencies: 227
-- Data for Name: cableelectrique; Type: TABLE DATA; Schema: StaRDT; Owner: -
--

-- TOC entry 4876 (class 0 OID 3384940)
-- Dependencies: 229
-- Data for Name: cabletelecommunication; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4878 (class 0 OID 3384954)
-- Dependencies: 231
-- Data for Name: canalisation; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4898 (class 0 OID 3385161)
-- Dependencies: 251
-- Data for Name: canalisationassainissementpluviale; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4897 (class 0 OID 3385154)
-- Dependencies: 250
-- Data for Name: canalisationeau; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4899 (class 0 OID 3385168)
-- Dependencies: 252
-- Data for Name: canalisationhydrocarburechimique; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4900 (class 0 OID 3385175)
-- Dependencies: 253
-- Data for Name: canalisationthermique; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4886 (class 0 OID 3385077)
-- Dependencies: 239
-- Data for Name: coffret; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4885 (class 0 OID 3385070)
-- Dependencies: 238
-- Data for Name: conteneur; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4904 (class 0 OID 3385206)
-- Dependencies: 257
-- Data for Name: cote; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4871 (class 0 OID 3384896)
-- Dependencies: 223
-- Data for Name: elementreseau; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4879 (class 0 OID 3384962)
-- Dependencies: 232
-- Data for Name: enveloppecableconduite; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4901 (class 0 OID 3385188)
-- Dependencies: 254
-- Data for Name: etiquette; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4880 (class 0 OID 3384969)
-- Dependencies: 233
-- Data for Name: fourreau; Type: TABLE DATA; Schema: StaRDT; Owner: -
--

-- TOC entry 4883 (class 0 OID 3385057)
-- Dependencies: 236
-- Data for Name: galerie; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4907 (class 0 OID 3385225)
-- Dependencies: 260
-- Data for Name: geometriesupplementaire_ligne2d; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4908 (class 0 OID 3385231)
-- Dependencies: 261
-- Data for Name: geometriesupplementaire_point2d; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4905 (class 0 OID 3385212)
-- Dependencies: 258
-- Data for Name: geometriesupplementaire_surface2d; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4906 (class 0 OID 3385218)
-- Dependencies: 259
-- Data for Name: geometriesupplementaire_surface3d; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4903 (class 0 OID 3385200)
-- Dependencies: 256
-- Data for Name: informationsupplementaire; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4882 (class 0 OID 3384983)
-- Dependencies: 235
-- Data for Name: nappecable; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4893 (class 0 OID 3385125)
-- Dependencies: 246
-- Data for Name: noeudreseau; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4872 (class 0 OID 3384905)
-- Dependencies: 224
-- Data for Name: ouvrage; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4911 (class 0 OID 3385273)
-- Dependencies: 264
-- Data for Name: ouvrageentechniquealternative; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4902 (class 0 OID 3385194)
-- Dependencies: 255
-- Data for Name: perimetreparticulier; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4910 (class 0 OID 3385243)
-- Dependencies: 263
-- Data for Name: pointleveouvragereseau; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4909 (class 0 OID 3385237)
-- Dependencies: 262
-- Data for Name: pointlevepcrs; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4887 (class 0 OID 3385084)
-- Dependencies: 240
-- Data for Name: poteau; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4895 (class 0 OID 3385140)
-- Dependencies: 248
-- Data for Name: protectioninondationsubmersion; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4881 (class 0 OID 3384976)
-- Dependencies: 234
-- Data for Name: protectionmecanique; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4888 (class 0 OID 3385091)
-- Dependencies: 241
-- Data for Name: pylone; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4889 (class 0 OID 3385098)
-- Dependencies: 242
-- Data for Name: regardnonvisitable; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4890 (class 0 OID 3385105)
-- Dependencies: 243
-- Data for Name: regardvisitable; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4892 (class 0 OID 3385119)
-- Dependencies: 245
-- Data for Name: reseauutilite; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 4873 (class 0 OID 3384912)
-- Dependencies: 225
-- Data for Name: troncon; Type: TABLE DATA; Schema: StaRDT; Owner: -
--
--
-- TOC entry 5036 (class 0 OID 0)
-- Dependencies: 222
-- Name: elementreseau_identifiant_seq; Type: SEQUENCE SET; Schema: StaRDT; Owner: -
--
SELECT pg_catalog.setval('"StaRDT".elementreseau_identifiant_seq', 1, true);
--
-- TOC entry 5037 (class 0 OID 0)
-- Dependencies: 265
-- Name: ouvrage_idouvrage_seq; Type: SEQUENCE SET; Schema: StaRDT; Owner: -
--
SELECT pg_catalog.setval('"StaRDT".ouvrage_idouvrage_seq', 1, true);
--
-- TOC entry 4732 (class 2606 OID 3384904)
-- Name: elementreseau elementreseau_pk; Type: CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".elementreseau
    ADD CONSTRAINT elementreseau_pk PRIMARY KEY (identifiant);
--
-- TOC entry 4734 (class 2606 OID 3385560)
-- Name: ouvrage ouvrage_un; Type: CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".ouvrage
    ADD CONSTRAINT ouvrage_un UNIQUE (idouvrage);
--
-- TOC entry 4739 (class 2606 OID 3385561)
-- Name: geometriesupplementaire_ligne2d geometriesupplementaire_ligne2d_fk; Type: FK CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".geometriesupplementaire_ligne2d
    ADD CONSTRAINT geometriesupplementaire_ligne2d_fk FOREIGN KEY (fk_elementreseau) REFERENCES "StaRDT".ouvrage(idouvrage) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- TOC entry 4740 (class 2606 OID 3385566)
-- Name: geometriesupplementaire_point2d geometriesupplementaire_point2d_fk; Type: FK CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".geometriesupplementaire_point2d
    ADD CONSTRAINT geometriesupplementaire_point2d_fk FOREIGN KEY (fk_elementreseau) REFERENCES "StaRDT".ouvrage(idouvrage) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- TOC entry 4737 (class 2606 OID 3385571)
-- Name: geometriesupplementaire_surface2d geometriesupplementaire_surface2d_fk; Type: FK CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".geometriesupplementaire_surface2d
    ADD CONSTRAINT geometriesupplementaire_surface2d_fk FOREIGN KEY (fk_elementreseau) REFERENCES "StaRDT".ouvrage(idouvrage) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- TOC entry 4738 (class 2606 OID 3385576)
-- Name: geometriesupplementaire_surface3d geometriesupplementaire_surface3d_fk; Type: FK CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".geometriesupplementaire_surface3d
    ADD CONSTRAINT geometriesupplementaire_surface3d_fk FOREIGN KEY (fk_elementreseau) REFERENCES "StaRDT".ouvrage(idouvrage) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- TOC entry 4736 (class 2606 OID 3385320)
-- Name: informationsupplementaire informationsupplementaire_fk; Type: FK CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".informationsupplementaire
    ADD CONSTRAINT informationsupplementaire_fk FOREIGN KEY (fk_elementreseau) REFERENCES "StaRDT".elementreseau(identifiant) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- TOC entry 4741 (class 2606 OID 3385314)
-- Name: pointleveouvragereseau pointleveouvragereseau_fk; Type: FK CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".pointleveouvragereseau
    ADD CONSTRAINT pointleveouvragereseau_fk FOREIGN KEY (fk_elementreseau) REFERENCES "StaRDT".elementreseau(identifiant) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- TOC entry 4735 (class 2606 OID 3385325)
-- Name: reseauutilite reseauutilite_fk; Type: FK CONSTRAINT; Schema: StaRDT; Owner: -
--
ALTER TABLE ONLY "StaRDT".reseauutilite
    ADD CONSTRAINT reseauutilite_fk FOREIGN KEY (fk_elementreseau) REFERENCES "StaRDT".elementreseau(identifiant) ON UPDATE CASCADE ON DELETE CASCADE;
-- Completed on 2022-04-23 23:12:34 CEST
--
-- PostgreSQL database dump complete
--
